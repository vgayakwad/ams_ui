import { Injectable, EventEmitter, Output, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AuthService } from 'angularx-social-login';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  @Output() getLoggedInName: EventEmitter<any> = new EventEmitter();
  head: any;
  sessionData: any;
  public baseUrl: string;
  constructor(private http: HttpClient, @Inject(LOCAL_STORAGE) 
  private storage: StorageService, private authService: AuthService,) {
  }

    /**
 * @author : Snehal
 * @function use : "get data of login user or register user from  local storage and store in httpheader"
 * @date : 10-10-2019
 */
  getlocalStorageData() {

    const loginLocalstorage = this.storage.get("login");
    const registerLocalstorage = this.storage.get("register");

    console.log('555555555555',loginLocalstorage);
    if (loginLocalstorage != null) {
      this.head = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': loginLocalstorage.AccessToken
      });
    }else if (registerLocalstorage != null) {
      this.head = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': registerLocalstorage.AccessToken
      });
    }
  }
  //end


  @Output() getRegisterUserName = new EventEmitter();

  /**
 * @author : Snehal
 * @param loginCredential
 * @function use : "post the login credential and after getting response from api, store the login data(Access Token) in local storage"
 * @date : 10-10-2019
 */
  Login(loginCredential): Observable<any> {
    return this.http.post('/Account/Login', loginCredential)
      .pipe(map(data => {
        this.storage.set('login', data);
        this.getLoggedInName.emit(data['FirstName'] + ' ' + data['LastName']);
        return data;
      }));
  }
  //end

  
 /**
 * @author : Snehal
 * @param   form
 * @function use : "post the registration form data and after getting response from api, store the register data in local storage"
 * @date : 14-11-2019
 */
  Register(form): Observable<any> {
    return this.http.post('/Account/Signup', form)
      .pipe(map(data => {      
        this.getRegisterUserName.emit(data);
        return data;
      }));
  }
  //end

 /**
 * @author : Snehal
 * @param   form
 * @function use : "post forgot password data with email,new password, confirm password."
 * @date : 14-10-2019
 */
  ResetPassword(form): Observable<any> {
    return this.http.post('/Account/ForgotPassword', form, { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }
  //end
  /**
   * @author : Smita
   * @function use : "CheckUserExist use for check user exist or not in database"
   * @date : 19-02-2020
   */
  CheckUserExist(email, provider):Observable<any>{
    return this.http.get('/Account/CheckUser?Email='+email+'&Provider='+provider).pipe(map(data =>{
      return data;
    }));
  } 



 /**
 * @author : Snehal
 * @function use : "called logout api and clear the local storage after logout."
 * @date : 12-10-2019
 */
  Logout(): Observable<any> {
   this.getlocalStorageData();
   console.log('7777777777777',this.head);
    return this.http.delete('/Account/Logout', { headers: this.head })
      .pipe(map(data => {
        
        this.storage.clear();
        this.storage.remove('login');
        this.storage.remove('register');
        this.authService.signOut();
        return data;
      }));
  }
  //end
}
