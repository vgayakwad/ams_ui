import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Location} from '@angular/common';
import { map, catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxUiLoaderConfig, NgxUiLoaderService } from 'ngx-ui-loader';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  head: any;
  public baseUrl: string;

  constructor(private http: HttpClient,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private ngxUiLoaderService: NgxUiLoaderService,
    @Inject(Location) private readonly location: Location) {

    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    console.log(registerUserData);
    if (registerUserData != null && registerUserData != undefined) {
      this.head = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': registerUserData.AccessToken
      });
    } else if (loginLocalStorage != null && loginLocalStorage != undefined) {
      this.head = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': loginLocalStorage.AccessToken
      });
    } else {
      this.head = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'dummyaccessToken'
      });
    }
  }


  allEventContent(eventPostObject) {
    
    console.log(eventPostObject);
    return this.http.post('/Event/GetEventList',eventPostObject, { headers: this.head })
      .pipe(map(data => {
        console.log('smitaaaaaaaaaaaaa',data);
        return data;
      }));
  }

  GetArtistList(artist_category_id,from_date,to_date) {
    //+'&fromDate='+from_date+'&toDate='+to_date
    let data = {
      'artistCategoryId':artist_category_id,
      'AvailableFrom':from_date,
      'AvailableTo':to_date
    }
    return this.http.post('/Event/GetArtistList',data, { headers: this.head })
      .pipe(map(data => {
        return data;
      }))
  }

  addEvents(data) {
    return this.http.post('/Event/AddEvent', data, { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }


  getEventDetails(eventDetailsId,isPast): Observable<any> {
    return this.http.get('/Event/GetEventDetails?Id=' + eventDetailsId+'&IsPast='+isPast, { headers: this.head })
      .pipe(map(data => {
        return data;
      }))
  }


  addEventGalleryPhotos(galleryObj): Observable<any> {
    return this.http.post('/Event/AddEventGallary', galleryObj, { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }

  getAllGalleryPhotos(eventId): Observable<any> {
    return this.http.get('/Event/GetGalleryImages?EventId=' + eventId)
      .pipe(map(data => {
        return data;
      }))
  }


  deleteGalleryPhoto(data): Observable<any> {
    const options = {
      headers: this.head,
      body: data,
    };
    return this.http.delete('/Event/DeleteGalleryImages', options)
      .pipe(map(data => {
        return data;
      }));
  }


   /**
   * @author : Snehal
   * @param searchEventTitle artistCategoryId dateFilter artistId
   * @function use : "get events of login artist and send parameters to search event."
   * @date : 14-01-2020
   */
  getArtistEventById(searchEventTitle,artistCategoryId,dateFilter,artistId): Observable<any> {
    const options = {
      SearchTitle: searchEventTitle,
      CategoryId: artistCategoryId,
      DateFormat:dateFilter,
      UserId:artistId
    };
    console.log(options)
    return this.http.post('/Event/GetEventByArtistId',options)
    .pipe(map(data => {
      return data;
    }))
  }

  /**
   * @author : Smita
   * @param : data 
   * @function use : "show artist/ organizer performed events list "
   * @date : 19-02-2020
   */
  getArtistOrganizerPastEvents(data):Observable<any>{
    return this.http.post('/Event/GetEventListForPastEvents',data)
    .pipe(map(data => {
      return data;
    }))
  }

  getTimeZoneList(countryCode): Observable<any> {
    return this.http.get('/Common/GetTimeZoneByCountryCode?countryCode=' + countryCode, { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }
  CountryList(): Observable<any> {
    return this.http.get('/Common/GetCountryList', { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }


  StateList(countryCode): Observable<any> {
    const url = "/Common/GetStateByCountryCode/?countryCode=" + countryCode;
    return this.http.get(url, { headers: this.head }).pipe(map(
      data => {
        return data;
      }
    ));

  }

  CityList(stateCode): Observable<any> {
    const url = "/Common/GetCityByStateCode/?stateCode=" + stateCode;
    return this.http.get(url, { headers: this.head }).pipe(map(
      data => {
        return data;
      }
    ));

  }

  AddArtistSchedule(form): Observable<any> {
    return this.http.post('/Schedule/AddArtistSchedule', form, { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }

  getAllArtistSchedule(): Observable<any> {
    return this.http.get('/Schedule/GetAllArtistSchedules', { headers: this.head })
      .pipe(map(data => {
        return data;
      }))
  }

  getArtistScheduleByArtistId(artistId): Observable<any> {

    return this.http.get('/Schedule/GetArtistSchedulesByArtistId?artistId=' + artistId)
      .pipe(map(data => {
        return data;
      }))
  }

  getArtistSchedulebyScheduleID(id): Observable<any> {
    const url = "/Schedule/GetScheduleByScheduleId?ScheduleId=" + id;
    return this.http.get(url, { headers: this.head })
      .pipe(map(data => {
        return data;
      }))
  }
  advanceSearchContent(searchKey): Observable<any> {
    return this.http.get('/Event/AdvancedSearch?searchTitle='+searchKey)
      .pipe(map(data => {
        return data;
      }))
  }

  searchEventByCountry(searchData): Observable<any>{
    return this.http.get('/Event/CountryWiseSearch?searchKey='+searchData)
    .pipe(map(data => {
      return data;
    }))
  }

  getEventListByOrganizer(eventPostObj): Observable<any> {

    return this.http.post('/Event/GetEventByOrganizerId',eventPostObj, { headers: this.head })
      .pipe(map(data => {
        return data;
    }))
    // return this.http.get('/Event/GetEventByOrganizerId?organizerId=' + organizerId)
    //   .pipe(map(data => {
    //     return data;
    //   }))
  }

    /**
   * @author : Snehal
   * @param id
   * @function use : "delete artist schedule by schedule Id service to call api(Not in use)"
   * @date : 12-12-2019
   */
  deletetArtistSchedulebyScheduleID(id): Observable<any> {
    const urlDelete = "/Schedule/DeleteScheduleByScheduleId?ScheduleId=" + id;
    return this.http.delete(urlDelete,  { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }

  // getAllOrganizerList(): Observable<any> {
  //   return this.http.get('/Event/GetOrganizerList')
  //     .pipe(map(data => {
  //       return data;
  //     }))
  // }

  deleteEvent(data):Observable<any>{

    const options = {
      headers: this.head,
      body: data,
    }; 
    return this.http.delete('/Event/DeleteEventByEventId', options)
    .pipe(map(data => {
      return data;
    }));
  }
 

    /**
   * @author : Snehal
   * @param searchOrganizer userType
   * @function use : "common search functinality service to call api"
   * @date : 15-01-2020
   */
  getSearchUser(searchOrganizer,userType):Observable<any>{
    const url = '/Account/SearchUser?';
    return this.http.get(url,{ params: {
      searchTitle: searchOrganizer,
      UserType: userType
      }
      })
    .pipe(map(data => {
      return data;
    }))
  }

  /**
   * @author : smita
   * @param skillId 
   * @function use : fetch events passing artist skill id 
   * @date : 17-1-2020
   */

  eventListByArtistSkills(skillId){
    return this.http.get('/Event/GetEventByArtistSkills?SkillId='+skillId).pipe(map(data =>{
      return data;
    }));

  }

  /**
   * @author : smita
   * @param artistId 
   * @function use : getArtistCategoryByArtistId use for get artist category passing artist id 
   * @date : 20-1-2020
   */
  getArtistCategoryByArtistId(artistId){
    return this.http.get('/Event/GetCategoryByArtistId?ArtistId='+artistId, { headers: this.head }).pipe(map(data =>{
      return data;
    }));
  }

  /**
   * @author : smita
   *  @param eventCategoryId 
   * @function use : eventListByCategoryId use for show category wise events
   */
  eventListByCategoryId(eventCategoryId,searchKey){    
    return this.http.get('/Event/GetEventListByEventCategoryWise?EventTypeId='+eventCategoryId+'&SearchTitle='+searchKey, { headers: this.head }).pipe(map(data =>{
      return data;
    }));
  }

  /**
   * @author : smita
   * @function use : eventCountByEventCategory use for show category wise events count on dashboard
   * @date : 23-1-2020
   */
  eventCountByEventCategory(){
    return this.http.get('/Event/GetEventCountByCategory').pipe(map(data =>{
      return data;
    }));    
  }

  /**
   * @Author : Smita
   * @function Use : 'addRating use for insert event, artist, or organizer rating on database
   * @date : 12-2-2020   
   */

  addRating(data){
    return this.http.post('/RateAndReview/AddUserRating', data).pipe(map(data =>{
      return data;
    }))

  }

  /**
   * @Author :smita
   * @function use : get system Ip
   * @date : 25-2-2020
   */
 
   getSystemIp(){ 
    return this.http.get("http://api.ipify.org/?format=json").pipe(map(data => {
      return data;
    }));   
   }

   getGEOLocation(latitude,longitude){   
    
    //let url = "https://api.ipgeolocation.io/ipgeo?apiKey=AIzaSyDrTdFn5n9tvR6qM6YFOjH7wswRvcz_khQ&ip="+ip; 
      return this.http.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+','+longitude+"&key=AIzaSyDrTdFn5n9tvR6qM6YFOjH7wswRvcz_khQ").pipe(map(data =>{
        return data;
      }));     
   }

  //end
}