import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  head: any;
  arrArtist: any;
  sessionData: any;
  sessionRegisterData: any;
  private currentDate = new Date();
  constructor(private httpClient: HttpClient,
    @Inject(LOCAL_STORAGE) private storage: StorageService) {

    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    if (registerUserData != null && registerUserData != undefined) {
      this.head = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': registerUserData.AccessToken
      });
    }
    if (loginLocalStorage != null && loginLocalStorage != undefined) {
      this.head = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': loginLocalStorage.AccessToken
      });
    }
  }

    /**
 * @author : Snehal
 * @function use : "Get artist category list."
 * @date : 22-10-2019
 */
  artistCategoryList(): Observable<any> {
    return this.httpClient.get('/Common/GetArtistCategory', { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }
  //end


  EventsCategoryList(): Observable<any> {
    return this.httpClient.get('/Common/GetEventCategory')
      .pipe(map(data => {
        return data;
      }));

  }


    /**
 * @author : Snehal
 * @function use : "get country list."
 * @date : 21-10-2019
 */
  CountryList(): Observable<any> {
    return this.httpClient.get('/Common/GetCountryList')
      .pipe(map(data => {
        return data;
      }));
  }
  //end

  /**
 * @author : Snehal
 * @param countryCode
 * @function use : "get state list by sending country code ."
 * @date : 22-10-2019
 */
  StateList(countryCode): Observable<any> {
    const url = "/Common/GetStateByCountryCode/?countryCode=" + countryCode;
    return this.httpClient.get(url, { headers: this.head }).pipe(map(
      data => {
        return data;
      }
    ));

  }
  //end

  /**
 * @author : Snehal
 * @param countryCode
 * @function use : "Get time zone list for country code."
 * @date : 22-10-2019
 */
  getTimeZoneList(countryCode): Observable<any> {
    return this.httpClient.get('/Common/GetTimeZoneByCountryCode?countryCode=' + countryCode, { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }
  //end


/**
 * @author : Snehal
 * @param stateCode
 * @function use : "Get city list by sending state code."
 * @date : 22-10-2019
 */
  CityList(stateCode): Observable<any> {
    const url = "/Common/GetCityByStateCode?stateCode=" + stateCode;
    return this.httpClient.get(url, { headers: this.head }).pipe(map(
      data => {
        return data;
      }
    ));

  }
  //end

  /**
 * @author : Snehal
 * @function use : "Get user profile details to populate data on view profile"
 * @date : 08-11-2019
 */
  getUserDetails(): Observable<any> {
    return this.httpClient.get('/Account/GetUserDetails/?profileId=0', { headers: this.head })
      .pipe(map(data => {
        console.log("sheeee", data);
        return data;
      }));
  }
  //end

   /**
 * @author : Snehal
 * @param form
 * @function use : "send user profile form data"
 * @date : 08-11-2019
 */
  PostUserDetails(form): Observable<any> {
    return this.httpClient.post('/Account/AddProfile', form, { headers: this.head })
      .pipe(map(data => {
        console.log("sheeee", data);
        return data;
      }));
  }
  //end

  
 /**
 * @author : Snehal
 * @function use : "get skill list from database"
 * @date : 06-01-2020
 */
  getSkillDropdownList(): Observable<any> {
    return this.httpClient.get('/Account/GetArtistSkills', { headers: this.head })
      .pipe(map(data => {
        return data;
      }));
  }
  //end

}//class end


