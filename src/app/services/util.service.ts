import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  ShowErrorMsg: String;
  errorMsg: String;
  isError: boolean = false;
  previewUrl: string = '';
  preViewData: any;
  constructor() { }
  getErrorMessagebyStatus(error) {
    this.isError = true;
    if (error.error) {
      switch (error.status) {
        case 400:
          if (error.error != '') {
            this.errorMsg = error.error.Message;
          } else {
            this.errorMsg = error.error;
          }
          break;
        case 404:
          if (error.error != '') {
            this.errorMsg = error.error;
          } else {
            this.errorMsg = error.error.Message;
          }
          console.log(this.errorMsg);
          break;
        case 500:
          if (error.error != '') {
            this.errorMsg = error.error.Message;
          } else {
            this.errorMsg = error.error;
          }
          //this.errorMsg = error.error;
          console.log(this.errorMsg);
          break;
        case 409:
          if (error.error != '') {
            this.errorMsg = error.error.Message;
          } else {
            this.errorMsg = error.error;
          }
          break;
        default:
          this.errorMsg = "Server could not be contacted."
          break;

      }
    }
    return this.errorMsg;
  }

  /**
 * @author : Snehal
 * @param password
 * @param confirmPassword
 * @function use : "Common Function for matching password and confirm password while registration and forgot password."
 * @date : 12-11-2019
 */
  MustMatch(password: string, confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const Password = formGroup.controls[password];
      const confirm_Password = formGroup.controls[confirmPassword];
      if (confirm_Password.errors && !confirm_Password.errors.mustMatch) {
        return;
      }
      let pass = Password.value.trim().toLowerCase();
      let confirmPass = confirm_Password.value.trim().toLowerCase();
      if (pass !== confirmPass) {
        confirm_Password.setErrors({ mustMatch: true });
      } else {
        confirm_Password.setErrors(null);
      }
    }
  }
  //end

  /**
 * @author : Snehal
 * @param file
 * @function use : "Common Function for Converting user uploaded profile image into base64 value."
 * @date : 12-11-2019
 */
  ImagePreview(file) {

    return new Promise((resolve, reject) => {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = reject;
    })
  }
  //end

}



