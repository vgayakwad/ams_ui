import { Injectable,Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
 
  constructor(private route: Router, @Inject(LOCAL_STORAGE) private storage: StorageService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {
      const loginLocalstorage=this.storage.get("login");
      // const loginLocalstorage = JSON.parse(localStorage.getItem('login'));
      console.log("local",loginLocalstorage);
      const registerLocalstorage=this.storage.get("register");
      // const loginLocalstorage = localStorage.getItem('login');
      // const Registerlocalstorage = localStorage.getItem('register');
        if (loginLocalstorage != null){
          return true;
        }
        else if(registerLocalstorage != null){
          // this.route.navigate(['/']);
          return true;
        }
        else{
          return false;
        }
  }
}
