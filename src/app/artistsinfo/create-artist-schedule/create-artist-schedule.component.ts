import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location, Appearance } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { DashboardService } from '../../services/dashboard.service';
import Swal from 'sweetalert2';
import {UtilService} from '../../services/util.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Component({
  selector: 'app-create-artist-schedule',
  templateUrl: './create-artist-schedule.component.html',
  styleUrls: ['./create-artist-schedule.component.css']
})
export class CreateArtistScheduleComponent implements OnInit {
  showAdd:boolean = false;
  createArtistForm:FormGroup;
  city_id: any;
  cityList:any;
  countryList:any;
  stateList:any;
  getState:any;
  getCity: any;
  getCountry = 'US';
  getTimeZoneOffset:any;
  timeZoneId:any;
  getPostalCode: any;
  allTimeZone:any;
  public latitude: number;
  public longitude: number;
  public selectedAddress: PlaceResult;
  public zoom: number;
  updatedAddress: any;
  public appearance = Appearance;
  errorMessage :any;
  errorMsgCmpDate: any;
  isError:boolean=false;
  ShowErrorMsg: any;
  localStorageLogin: any;
  localStorageRegister: any;
  userStorageData: any;
  scheduleId:any;
  showUserSchedule:any;
  showScheduleLocation: Boolean = false;
  FromDate:any;
  ToDate:any;
  submitted:boolean = false;
  artistId:any;
  public minDate = moment(new Date()).format("YYYY-MM-DD");
   
  constructor(private formBuilder: FormBuilder,private dashboardService: DashboardService,
    private utilService:UtilService,private ngxLoader: NgxUiLoaderService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.ngxLoader.start();
    this.scheduleId = this.route.snapshot.paramMap.get('paramA');
    this.localStorageLogin = this.storage.get("login");
    this.localStorageRegister = this.storage.get("register");
    if (this.localStorageLogin != null) {
      this.userStorageData = this.localStorageLogin;
      this.artistId = btoa(this.userStorageData.Id);
   }
    if (this.localStorageRegister != null) {
      this.userStorageData = this.localStorageRegister;
      this.artistId = btoa(this.localStorageRegister.Id);
    }
   
    if(this.scheduleId != "newSchedule"){
      this.scheduleId = atob(this.route.snapshot.paramMap.get('paramA'));
      this.getPopulateData(this.scheduleId);
    }
    else if( this.city_id == undefined && this.getState == undefined){
      this.city_id = "";
      this.getState ="";
    }
    this.onCountryChange(this.getCountry);
    this.getCountryList();
 
    this.createArtistForm= this.formBuilder.group({
      AvailableFrom:['',[Validators.required]],
      AvailableTo:['',[Validators.required]],
      CountryCode:['',[Validators.required]],
      StateCode:['',[Validators.required]],
      CityId:['',[Validators.required]],
      Address: ['']
    });
  }
  /**
   * @author : Snehal
   * @function use : "get form control name to apply validation in front end(Not in use)"
   * @date : 10-12-2019
   */
  get f() { return this.createArtistForm.controls; }
  //end


   /**
   * @author : Snehal
   * @param scheduleId
   * @function use : "populate data of schedule in edit mode(Not in use) "
   * @date : 06-12-2019
   */
  getPopulateData(scheduleId){

    this.dashboardService.getArtistSchedulebyScheduleID(scheduleId).subscribe(res=>{
      this.ngxLoader.stop();
      if(Object.keys(res).length > 0){
        this.showUserSchedule = res;
        console.log("ava date",this.showUserSchedule);
        if (this.showUserSchedule.AvailableFrom != null && this.showUserSchedule.AvailableTo != null) {
          this.FromDate = new Date(this.showUserSchedule.AvailableFrom);
          this.ToDate =  new Date(this.showUserSchedule.AvailableTo);
        }
          if (this.showUserSchedule.StateCode != null) {
            this.onStateChange(this.showUserSchedule.StateCode);
          }
          this.city_id = this.showUserSchedule.CityId;
          if (this.showUserSchedule.Address != "" && this.showUserSchedule.Address != undefined) {
            this.showScheduleLocation = true;
          }

        }  
    },error => {
      this.ngxLoader.stop();
      this.handleError(error);
     });
  }
  //end
 
     /**
   * @author : Snehal
   * @function use : "set  flag for hide the  border-bottom line of address(Not in use)."
   * @date : 24-11-2019
   */
  showScheduleAddress() {
    this.showScheduleLocation = false;
  }
  //end

   /**
   * @author : Snehal
   * @function use : "get country list for dropdown(Not in use)"
   * @date : 25-11-2019
   */
  getCountryList(){  
    this.dashboardService.CountryList().subscribe((data)=>{ 
      this.ngxLoader.stop();
      if(data.length > 0 ) 
      {
        this.countryList = data ; 
      }
      else{
        alert("Countrylist is not found");
      }   
            
     },error => {
      this.ngxLoader.stop();
      this.handleError(error);
     });
  }
  //end

     /**
   * @author : Snehal
   * @param SelCountryCode
   * @function use : "on country change get state list for dropdown(Not in use)."
   * @date : 25-11-2019
   */
  onCountryChange(SelCountryCode){ 
      this.getCountry = SelCountryCode;
      this.dashboardService.StateList(this.getCountry).subscribe((data:any)=>{ 
      if(data.length>0){
        this.stateList = data ;
      }
      else{
        alert("Statelist is not found");
      }   
      },error => {
        this.handleError(error);
       });

 }
 //end

    /**
   * @author : Snehal
   * @param SelStateCode
   * @function use : "on state change get city list for dropdown(Not in use)"
   * @date : 25-11-2019
   */
 onStateChange(SelStateCode){
  this.city_id="";
  this.getState = SelStateCode;
    this.dashboardService.CityList(this.getState).subscribe((data:any)=>{
    if(data.length>0) 
    {
      data.filter(element => {
        if (element.CityName == this.getCity) {
          this.city_id = element.Id;
        }
      });
      this.cityList = data ;
    } 
    else{
      alert("Citylist is not found");
    }
    
    },error => {
      this.handleError(error);
     }); 
}
//end

 /**
   * @author : Snehal
   * @param result
   * @function use : "get the state and city on selection of address from google-map(Not in use) "
   * @date : 25-11-2019
   */
  onAutocompleteSelected(result: PlaceResult) {
    if (result.address_components != null) {
     
      this.createArtistForm.controls['Address'].setValue(result.formatted_address);
      for (var i = 0; i < result.address_components.length; i++) {
        var addr = result.address_components[i];
        switch (addr.types[0]) {
          case 'locality': this.getCity = addr.long_name;
            break;
          case 'administrative_area_level_1': this.getState = addr.short_name;
            break;
          case 'country': this.getCountry = addr.short_name;
            break;
          case 'postal_code': this.getPostalCode = addr.long_name;
            break;
          default:
        }   
        if (this.getCountry != '' && this.getCountry != undefined) {
          this.onCountryChange(this.getCountry);
  
        }
        if (this.getState != '' && this.getState != undefined) {
          this.onStateChange(this.getState);
      }
      }  
    } else {
      alert('Address not found');
    }
  }
  //end

  

    /**
   * @author : Snehal
   * @param error
   * @function use : "Client and Server side Error handling(Not in use) "
   * @date : 13-11-2019
   */
  handleError(error) {
    if (error.error instanceof ErrorEvent) {
      this.errorMessage = `Error: ${error.error.message}`;
     } else {
       this.ShowErrorMsg= this.utilService.getErrorMessagebyStatus(error);
       this.errorMessage = this.ShowErrorMsg;// `${error.error.message}`;
      const Toast = Swal.mixin({
       toast: true,
       showConfirmButton: false,
       timer: 3000
     })
     Toast.fire({
       icon: 'error',
       title: this.errorMessage
     });
  
     }
   }
   //end


     /**
   * @author : Snehal
   *  @param form
   * @function use : "post artist schedule details on submit button(Not in use)."
   * @date : 26-11-2019
   */
   createArtistSchedule(form){
    this.ngxLoader.start();
    this.submitted=true;
    if(form.value.AvailableTo < form.value.AvailableFrom){
      this.isError = true;
      this.errorMsgCmpDate = "To Date must be greater than from date.";
    }else{
      this.isError = false;
      this.errorMsgCmpDate="";
    }
    if (form.value.Address == "" && form.value.Address != undefined && this.scheduleId!= 'newSchedule') {
      form.value.Address = this.showUserSchedule.Address;
    }

    if(this.scheduleId!= '' && this.scheduleId!=undefined && this.scheduleId!= 'newSchedule'){
      form.value['ScheduleId'] = this.scheduleId;
    }

    if(form.valid && this.isError != true){
      form.value.AvailableFrom = form.value.AvailableFrom.toISOString();
      form.value.AvailableTo = form.value.AvailableTo.toISOString();
      form.value.Status = "AVAILABLE";
      form.value.ArtistId = this.userStorageData.Id;
      this.dashboardService.AddArtistSchedule(form.value).subscribe(res=>{
        this.ngxLoader.stop();
        if(res.IsSuccess == true){
          const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 3000
          })
          Toast.fire({
            icon: 'success',
            title:  (this.scheduleId != '' && this.scheduleId != undefined && this.scheduleId == 'newSchedule')?'Artist Schedule Created Successfully.':'Artist Schedule Updated Successfully.'
          });
          this.router.navigate(['/artist/',this.artistId]);
        }
        else {
          alert('Artist Schedule not update');
        }  
      }, error => {
        this.ngxLoader.stop();
        this.handleError(error);
      });
    }
    else{
      this.ngxLoader.stop();
    }
     
   }
   //end
}
