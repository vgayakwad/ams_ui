import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateArtistScheduleComponent } from './create-artist-schedule.component';

describe('CreateArtistScheduleComponent', () => {
  let component: CreateArtistScheduleComponent;
  let fixture: ComponentFixture<CreateArtistScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateArtistScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateArtistScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
