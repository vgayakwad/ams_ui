import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import Swal from 'sweetalert2';
import { UtilService } from '../../services/util.service';
import { Observable, throwError, Subscription } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css'],
  providers: [DashboardService, UtilService],
})
export class ArtistComponent implements OnInit {
  artistForm: FormGroup;
 // eventScheduleList: any
  artistUserInfo:any;
  p: number = 1;
  ShowErrorMsg: any;
  loginLocalStorage: any;
  registerLocalStorage: any;
  loginUserID: any;
  loginEmailId:any;
  //ShowErrorMsgSchedule: any;
  artistId: any;
 // selectedArtistId:any;
  //routeSubscription: any;
  artistName:any;
  contactedEmailId :any;
  // error: any = {
  //   isErrorArtistDescFlag: false,
  //   errorMessageArtistDesc: '',
    
  // };
 // errors: any = {};
  SearchKey:any;

  constructor(private dashboardService: DashboardService,
    private ngxLoader: NgxUiLoaderService,
    public route: ActivatedRoute,
    private router: Router, private utilService: UtilService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private formBuilder: FormBuilder) {
  }


  ngOnInit() {
    this.ngxLoader.start();
    this.loginLocalStorage = this.storage.get("login");
    this.registerLocalStorage = this.storage.get("register");
    if (this.loginLocalStorage != null && this.loginLocalStorage != undefined) {
      this.loginUserID = this.loginLocalStorage.Id;
      this.loginEmailId = this.loginLocalStorage.Email
    }
    else if (this.registerLocalStorage != null && this.registerLocalStorage != undefined) {
      this.loginUserID = this.registerLocalStorage.Id;
      this.loginEmailId = this.registerLocalStorage.Email;
    }
   // this.checkArtistID();
    this.getAllArtistList('');
    this.artistForm = this.formBuilder.group({
      artist_name: [''],
      artist_emailId:[''],    
      description: ['', Validators.required],
     
    }, {

    });
  }

  get f() { return this.artistForm.controls; }

  // checkArtistID() {
  //   this.routeSubscription = this.route.params.subscribe((param: any) => {
  //     console.log(param);
  //     this.artistId = param['paramA'];
  //     if (param['paramA'] == 'all') {
  //       this.allArtistSchedule();
  //     } else {
  //       this.artistId = atob(param['paramA']);
  //       this.artistScheduleById(this.artistId);
  //     }
  //   });
  // }


 /**
   * @author : Snehal
   * @function use : "get selected search value of artist from search-bar component"
   * @date : 16-01-2020.
   */

  getAllArtistList(searchKey){
 
    //this.ngxLoader.start();
    let userType = 'Artist';
    //this.SearchKey = searchKey;
    this.dashboardService.getSearchUser(searchKey, userType).subscribe(response =>{
      this.ngxLoader.stop();  
      if(response.length>0){        
        this.artistUserInfo = response;
        console.log('66666666',this.artistUserInfo);
      }
    }, error => {
     // this.ngxLoader.stop();  
      this.handleError(error);
    });

  }

  /**
   * @author : smita
   * @function use : showArtistEventList function use for show artist list with assigned event list
   * @date : 16-1-2020
   */
  showArtistEventList(artist_id){
    this.router.navigate(['/artist-events/', btoa(artist_id)]);
    
  }

  allArtistSchedule() {
    // this.ngxLoader.start();
    // this.ShowErrorMsgSchedule = '';
    // this.eventScheduleList = [];
    // this.dashboardService.getAllArtistSchedule().subscribe(res => {
    //   this.ngxLoader.stop();
    //   if (Object.keys(res).length > 0) {
    //     this.eventScheduleList = res;
    //     console.log("get", this.eventScheduleList);
    //     this.eventScheduleList.filter(item => {
    //       if (item.ArtistId == this.loginUserID) {
    //         item['AllowUserEditSchedule'] = true;
    //       }
    //       else {
    //         item['AllowUserEditSchedule'] = false;
    //       }
    //     });
    //     this.eventScheduleList = this.eventScheduleList.sort(function (a, b) {
    //       return b.AllowUserEditSchedule - a.AllowUserEditSchedule
    //     })
    //   }
    //   console.log("get....", this.eventScheduleList);
    // }, error => {
    //   this.ngxLoader.stop();
    //   this.eventScheduleList = [];
    //   this.handleError(error);
    // });

  }

  artistScheduleById(artistID) {
    // this.ShowErrorMsgSchedule = '';
    // this.eventScheduleList = [];
    // this.dashboardService.getArtistScheduleByArtistId(artistID).subscribe(result => {
    //   this.ngxLoader.stop();
    //   console.log("result", result);
    //   if (Object.keys(result).length > 0) {
    //     this.eventScheduleList = result;
    //     this.eventScheduleList.filter(item => {
    //       if (item.ArtistId == this.loginUserID) {
    //         item['AllowUserEditSchedule'] = true;
    //       }
    //       else {
    //         item['AllowUserEditSchedule'] = false;
    //       }
    //     });
    //     this.eventScheduleList = this.eventScheduleList.sort(function (a, b) {
    //       return b.AllowUserEditSchedule - a.AllowUserEditSchedule
    //     })
    //   }
    // }, error => {
    //   this.ngxLoader.stop();
    //   this.eventScheduleList = [];
    //   this.ShowErrorMsgSchedule = error.error.Message;
    //   // this.handleError(error);
    // })
  }

  // editArtistSchedule(eachSchedule) {
  //   this.router.navigate(['/create-artist-schedule', btoa(eachSchedule.ScheduleId)]);
  // }

  // deletetArtistSchedulebyScheduleID(eachScheduleDelete) {
  //   const swalWithBootstrapButtons = Swal.mixin({     
  //   })   
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: 'You want to delete this Schedule?',
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonText: 'Yes, delete it!',
  //     cancelButtonText: 'No, keep it'
  //   }).then((result) => {
  //     if (result.value) {
  //       this.dashboardService.deletetArtistSchedulebyScheduleID(eachScheduleDelete.ScheduleId).subscribe(res => {
  //         if (res.IsSuccess == true) {
  //           this.checkArtistID();
  //           swalWithBootstrapButtons.fire(
  //             'Artist Schedule has been deleted.',
  //             '',
  //             'success'
  //           )
  //         }
  //       }, error => {
  //         this.handleError(error);
  //       });
  //     } else if (result.dismiss === Swal.DismissReason.cancel) {
  //       swalWithBootstrapButtons.fire(
  //         'Cancelled',
  //         '',
  //         'error'
  //       )
  //     }
  //   });
  // }

  /**
   * @author : smita
   * @function use : populateArtistInfo function use for populate artist name, email id on popup
   * @date : 3-1-2020
   */
  // populateArtistInfo(ScheduleId) {

  //   if (ScheduleId != null && ScheduleId != undefined) {
  //     this.eventScheduleList.filter(items => {       
  //       if (ScheduleId == items.ScheduleId) {         
  //         this.artistName = items.FirstName+ ' '+ items.LastName;
  //         this.contactedEmailId = (items.ContactEmail!=null && items.ContactEmail!= undefined)?items.ContactEmail:items.Email;
  //         this.selectedArtistId = items.ArtistId;
  //       }
  //     })  
  //   }
  // }

  /**
   * @author : smita
   * @function use : sendRequestToArtist function use for send mail to artist
   * @date : 3-1-2020
   */
  // sendRequestToArtist(){

  //   console.log('test', this.artistForm.value);

  //   if (this.artistForm.controls['description'].value == undefined 
  //   || this.artistForm.controls['description'].value == '') {
  //     this.errors['isErrorArtistDescFlag'] = true;
  //     this.errors['errorMessageArtistDesc'] = 'Message is required.'
  //     this.error = this.errors;
  //   } else {
  //     this.errors['isErrorArtistDescFlag'] = false;
  //     this.error = this.errors;
  //   }

  //   let checkErrorStatus = 0;
  //   for (var i in this.errors) {
  //     if (this.errors[i] == true) {
  //       checkErrorStatus = 1
  //     }
  //   }
  //   let postRequest = {};
  //   if(checkErrorStatus == 0 && Object.keys(this.artistForm.value).length > 0){
  //      postRequest['artistId'] =this.selectedArtistId ;
  //      postRequest['artistName']= this.artistForm.value.artist_name;
  //      postRequest['toEmailId'] =this.artistForm.value.artist_emailId;
  //      postRequest['message'] = this.artistForm.value.description;
  //      postRequest['subject'] ='';
  //      postRequest['organizerId'] =this.loginUserID;
  //      postRequest['fromEmailId'] = this.loginEmailId;

  //      console.log('postRequest===',postRequest);
  //   }

  // }



  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      });
    }
    return throwError(errorMessage);
  }

}
