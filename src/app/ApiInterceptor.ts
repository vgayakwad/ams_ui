import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable, of } from 'rxjs';


/** Inject With Credentials into the request */
@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const baseUrl = document.getElementsByTagName('base')[0].href;
    const apiBaseUrl = baseUrl.substring(0, (baseUrl.lastIndexOf(':')));  // localhost     
    //  const apiReq = req.clone({ url: `${apiBaseUrl}:9001${req.url}` });  // req.url is end point path  
   // console.log('55555555555555reqqqqqqq===', req.url.split('/'));
    const apiReq = (req.url == 'http://api.ipify.org/?format=json') ?
      req.clone({ url: `${req.url}` })
      : (req.url.split('/')[2] == 'maps.googleapis.com') ?
        req.clone({ url: `${req.url}` })
        : req.clone({ url: 'http://192.168.1.188:8060/api' + `${req.url}` });


    return next.handle(apiReq);

    //  return next.handle(request)
    //  .pipe(
    //    retry(1),
    //    catchError((error: HttpErrorResponse) => {
    //      let errorMessage = '';
    //      if (error.error instanceof ErrorEvent) {
    //        // client-side error
    //        errorMessage = `Error: ${error.error.message}`;
    //      } else {
    //        // server-side error
    //        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    //      }
    //      window.alert(errorMessage);
    //      return throwError(errorMessage);
    //    })
    //  )


  }

}




