import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
fullYear : any;
  constructor() { }

  ngOnInit() {
    this.fullYear =(new Date()).getFullYear()
  }

}
