
export class Employees {
	id : number;
	event_name: string;
	from_date_time : string;
	to_date_time: string;
	venue_details : string;
    artist_name : string;
    artist_category : string;
    contact_person_name : string;
    contact_number :string;
}