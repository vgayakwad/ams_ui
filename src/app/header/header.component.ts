import { Component, OnInit,Inject } from '@angular/core';
import { Location } from "@angular/common";
import { Router, ActivatedRoute,NavigationEnd} from '@angular/router';
import {AuthServiceService} from '../services/auth-service.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import Swal from 'sweetalert2';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { map, catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
//import { AuthService } from 'angularx-social-login';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [AuthServiceService],
})
export class HeaderComponent implements OnInit {

  loginUserFullName: String="";
  showLogin: Boolean=true;
  userName: string;
  showDropdown: Boolean=true;
  userType:any;
  profileId : any = btoa('0'); //zero convert in encode format 
  loginId:any;
  urlEndPath:any;
  currentUrl:any;
  constructor(private route: ActivatedRoute, private router: Router,
    location: Location,
    
    private authenticationService: AuthServiceService,
     @Inject(LOCAL_STORAGE) private storage: StorageService,
  private ngxLoader: NgxUiLoaderService) {
      
    router.events.subscribe((res :any) => {
      if (event instanceof NavigationEnd) {
       this.currentUrl = res.url;  
        console.log(this.router.url,"Current URL");  
      }
    });

    

    this.urlEndPath = router.url; // to print only path eg:"/"
    //console.log('7777==header---',router.url )
   authenticationService.getRegisterUserName.subscribe(data=>{
     console.log("h data",data);
     this.loginId = btoa(data.Id);
     this.userType = data['UserType']
     if((data['FirstName']+' '+data['LastName']) != ""){
      this.loginUserFullName = data['FirstName']+' '+data['LastName'];
      console.log("name",this.loginUserFullName);
      this.showLogin = false;
      this.showDropdown = true;
     }
     else{
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: "user register name not found"
      });
     }
    });
  }

  ngOnInit() {   
    this.loginUserFullName="";
    this.userType="";
    const loginLocalStorage =  this.storage.get("login");
    const registerLocalStorage =this.storage.get("register");
    const currentUrlStorage = this.storage.get('currentUrl');
    //alert(currentUrlStorage);
    if (loginLocalStorage != null && loginLocalStorage!= undefined){
        this.loginUserFullName = loginLocalStorage.FirstName+' '+loginLocalStorage.LastName;
        this.userType = loginLocalStorage.UserType;
        this.loginId = btoa(loginLocalStorage.Id);
    }
    else if(registerLocalStorage != null && registerLocalStorage != undefined){
      this.loginUserFullName = registerLocalStorage.FirstName+' '+registerLocalStorage.LastName;
      this.userType = registerLocalStorage.UserType;
      this.loginId = btoa(registerLocalStorage.Id);
    }
    else{
      this.showDropdown=false;
    }
    if(this.loginUserFullName != ""){
      this.showLogin = false;
    }
    
  }

     /**
   * @author : Snehal
   * @function use : "called logout api and clear session store in local storage."
   * @date : 12-10-2019
   */
  logout(){
    this.ngxLoader.start();
    this.authenticationService.Logout().subscribe(logoutres=>{
      console.log("logout response",logoutres)
      this.ngxLoader.stop();
      if(logoutres.IsSuccess === true){        
        this.storage.clear();
        this.storage.remove('login');
        this.storage.remove('register'); 
        //this.authService.signOut();       
        const Toast = Swal.mixin({
          toast: true,
          showConfirmButton: false,
          timer: 3000
        })
        Toast.fire({
          icon: 'success',
          title: "Sign out successfully"
        });
        
        this.router.navigate(['/Login']);
      }
      else{
        alert("Unable to logout");
      }
  
    },error => {
      this.ngxLoader.stop();
       this.handleError(error);
     });
    }
    //end

   
    
  handleError(error) {

    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', errorMessage);
      // window.alert(errorMessage);
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: errorMessage
      });
    } else {
      // server-side error
     // this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     // console.log('server-side error', this.ShowErrorMsg);
     // let showErrorMessage = (this.ShowErrorMsg.message != '') ? this.ShowErrorMsg.message : this.ShowErrorMsg;
    //  window.alert(showErrorMessage);
      // const Toast = Swal.mixin({
      //   toast: true,
      //   showConfirmButton: false,
      //   timer: 3000
      // })
      // Toast.fire({
      //   icon: 'error',
      //   title: this.ShowErrorMsg 
      // })
    }
    return throwError(errorMessage);
  }

}
