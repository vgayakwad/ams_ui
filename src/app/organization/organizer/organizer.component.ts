import { Component, OnInit,Inject,EventEmitter,Output } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import Swal from 'sweetalert2';
import { UtilService } from '../../services/util.service';
import { Observable, throwError,Subscription } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.css']
})
export class OrganizerComponent implements OnInit {

  organizerList:any;
  p: number = 1;
  ShowErrorMsg:any;
  ShowErrorMsgOrganizer: any;
  organizerPopForm:FormGroup;
  submitted:boolean = false;
  organizerName:any;
  contactEmailId:any;
  description:any;

  constructor(private dashboardService: DashboardService,
    private ngxLoader: NgxUiLoaderService,
    public route: ActivatedRoute,
    private router: Router,private utilService: UtilService,
     @Inject(LOCAL_STORAGE) private storage: StorageService,
     private formBuilder: FormBuilder) { }

  ngOnInit() {
   // this.getAllOrganizerList();
   this.getSearchUserList('');
    this.organizerPopForm = this.formBuilder.group({
      organizerName: [''],
      organizerEmailId:[''],    
      OrganizerDescription: ['', Validators.required], 
    });
  }

   /**
   * @author : Snehal
   * @function use : "get control names of organizer form"
   * @date : 12-12-2019
   */
  get f() { return this.organizerPopForm.controls; }
  //end



   /**
   * @author : Snehal
   * @function use : "get selected search value of organizer from search-bar component"
   * @date : 16-01-2020.
   */
  getSearchUserList(selectedOrganizer){
    this.ngxLoader.start();
    console.log("selected.....",selectedOrganizer);
      this.dashboardService.getSearchUser(selectedOrganizer,'Organizer').subscribe(searchData=>{
        this.ngxLoader.stop();
        this.organizerList = searchData;
      },error =>{
        this.ngxLoader.stop();
        this.handleError(error);
      })
  }
  //end


   /**
   * @author : Snehal
   * @function use : "Get all organizer detail list to show organizer data on cards"
   * @date : 18-12-2019
   */
  /**smita skip below function becoz same data get in getSearchUserList function 16-1-2020 */
  // getAllOrganizerList(){
  //   this.ngxLoader.start();
  //   this.organizerList = [];
  //   this.dashboardService.getAllOrganizerList().subscribe(res=>{
  //     this.ngxLoader.stop();
  //     console.log("all organizer",res);
  //     if (Object.keys(res).length > 0) {
  //       this.organizerList = res;
  //     }      
  //   }, error => {
  //     this.ngxLoader.stop();
  //     this.organizerList =[];
  //     this.ShowErrorMsgOrganizer = error.error.Message;
  //     // this.handleError(error);
  //   })
  // }
  //end


    /**
   * @author : Snehal
   * @function use : "on click of each organizer card navigate to organizer details screen to show organizer information"
   * @date : 14-01-2020
   */
  showOrganizerDetails(id){
    this.router.navigate(['/organizer-events/', btoa(id)]);
  }
  //end


    /**
   * @author : Snehal
   * @function use : "pop model for organizer for send enquiry(Not in use)"
   * @date : 06-01-2020
   */
  populateOrganizerInfo(organizerId) {
    this.description = "";
    this.organizerPopForm.controls['OrganizerDescription'].setValidators([Validators.required]);
    this.organizerPopForm.controls["OrganizerDescription"].updateValueAndValidity();
    if (organizerId != null && organizerId != undefined) {
      this.organizerList.filter(items => {
        if (organizerId == items.Id) {
          this.organizerName = items.FirstName + ' ' + items.LastName;
          this.contactEmailId = (items.ContactEmail != null && items.ContactEmail != undefined) ? items.ContactEmail : items.Email;
        }
      })
    }
  }
  //end

    /**
   * @author : Snehal
   * @function use : "send enquiry to organizer form(Not in use)"
   * @date : 06-01-2020
   */
  sendEnquiryToOrganizer(form){
    this.submitted =true;
  }
  //end

    /**
   * @author : Snehal
   * @function use : "cancel popup model form of send enquiry(Not in use)"
   * @date : 06-01-2020
   */
  cancelEnquiry(){
    this.organizerPopForm.controls['OrganizerDescription'].clearValidators();
    this.organizerPopForm.controls['OrganizerDescription'].setErrors(null);
  }
  //end

 /**
   * @author : Snehal
   * @function use : "Client and Server side Error handling "
   * @date : 13-11-2019
   */
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      });
    }
    return throwError(errorMessage);
  }
  //end
}
