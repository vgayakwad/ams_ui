import { Component, OnInit, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  Loginchecker = false;
  userType: any;
  eventButtonFlag: boolean = true;
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService,
  private route: ActivatedRoute,private router: Router,) { }

  ngOnInit() {
    const loginLocalStorage = this.storage.get('login');
    const registerLocalStorage = this.storage.get('register');
    if (loginLocalStorage != null && loginLocalStorage != undefined) {
      this.Loginchecker = true;
      this.userType = loginLocalStorage.UserType;
    } else if (registerLocalStorage != null && registerLocalStorage != undefined) {
      this.Loginchecker = true;
      this.userType = registerLocalStorage.UserType;
    }

  }

  screenRedirect(){
    this.router.navigate(['/events/',btoa('new'),btoa('false')]);   
  }

}
