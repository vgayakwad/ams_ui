import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EventDetailsComponent } from './event-details/event-details.component'
import {AuthGuard} from '../auth.guard';

const routes: Routes = [
  {path: "Dashboard", canActivate:[AuthGuard] ,component: EventDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
