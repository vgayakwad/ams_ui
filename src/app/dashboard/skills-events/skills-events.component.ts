import { Component, OnInit,Inject,Renderer } from '@angular/core';
import { ActivatedRoute, Params, Router , NavigationEnd} from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';
import { UtilService } from '../../services/util.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { Observable, throwError, from } from 'rxjs';

@Component({
  selector: 'app-skills-events',
  templateUrl: './skills-events.component.html',
  styleUrls: ['./skills-events.component.css'],
  providers: [DashboardService, UtilService],
})
export class SkillsEventsComponent implements OnInit {
  artistSkillId:any;
  SkillWiseEventList:any;
  SkillWiseEventListLength:any;
  ShowErrorMsg:any;
  constructor(public route: ActivatedRoute,
    private dashboardService: DashboardService,
    private utilService: UtilService,   
    private ngxLoader: NgxUiLoaderService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private render: Renderer,
    private router: Router,) {

      this.artistSkillId = atob(this.route.snapshot.paramMap.get('paramA'));
     }

  ngOnInit() {
    window.scroll(0,0);
    this.getEventListByArtistSkills();
   
  }

  /**
   * @author:smita
   * @function Use : getEventListByArtistSkills function use for show all events by passing artist skill id
   * @date : 17-1-2020
   */

  getEventListByArtistSkills(){

    if(this.artistSkillId!=null && this.artistSkillId!='' && this.artistSkillId!=undefined){
     
      this.dashboardService.eventListByArtistSkills(this.artistSkillId).subscribe(response =>{
        console.log('test skill', response)
        if(Object.keys(response).length > 0){
          this.SkillWiseEventList = response;
          this.SkillWiseEventListLength = Object.keys(response).length;
        }
      }, error =>{
        this.handleError(error);
      });
    }
  }

  clickEvent(eventId){
    console.log('event id', eventId)
    this.router.navigate(['/event-details', btoa(eventId)]);
  }

  handleError(error) {

    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', errorMessage);
      // window.alert(errorMessage);
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: errorMessage
      });
    } else {
      // server-side error
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      console.log('server-side error', this.ShowErrorMsg);
      let showErrorMessage = (this.ShowErrorMsg.message != '') ? this.ShowErrorMsg.message : this.ShowErrorMsg;
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      })
    }
    return throwError(errorMessage);
  }

 












}
