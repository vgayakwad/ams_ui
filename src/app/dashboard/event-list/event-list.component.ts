import { Component, OnInit, Renderer, Inject, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DashboardService } from '../../services/dashboard.service';
import { ProfileService } from '../../services/profile.service';
import { UtilService } from '../../services/util.service';
import { Observable, throwError, from } from 'rxjs';
import { map, catchError, retry, startWith } from 'rxjs/operators';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { log } from 'util';
import { Location, Appearance } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
declare var $: any;

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css'],
  providers: [DashboardService, UtilService, ProfileService],
  encapsulation: ViewEncapsulation.None
})
export class EventListComponent implements OnInit {

  advanceSearch = new FormControl();
  eventsResponse: any;
  advSearchResponse: any;
  eventDataLength: any;
  upPendingResponse: any;
  p: number = 1;
  userData: any[] = [];
  lastkeydown1: number = 0;
  errorMessage: any;
  ShowErrorMsg: any;
  eventAttribute: any;
  Loginchecker = false;
  eventType: any = 'upComing';
  eventStatus: boolean = false;
  eventTabName: any;
  loginId: any;
  userType: any;
  groupList: any;
  items: any;
  public latitude: number;
  public longitude: number;
  latLongCountry: any;
  //showAdvanceSearch = false;
  countriesData: any;
  selectedCountry: any;
  dashboardSearch: any = null;
  selectedCity: any;
  selectedState: any;
  eventsCategoryData: any;
  advanceSearchKey: any;
  ratingNumber: any = 0;
  ipAddress: any;
  // isDateFilter:any = null;
  dateFilterRadioSelected: any = 'IsWeek';
  dateFilter: any = [{
    'name': 'This Week',
    'value': 'IsWeek'
  },
  {
    'name': 'Today',
    'value': 'IsToday'
  },
  {
    'name': 'Tomorrow',
    'value': 'IsTomorrow'
  },
  {
    'name': 'This Weekend',
    'value': 'IsWeekend'
  },
  {
    'name': 'This Month',
    'value': 'IsMonth'
  }
  ];
  dateFilterForPastEvent: any = [{
    'name': 'Last Week',
    'value': 'IsWeek',
  }, {
    'name': 'Last Month',
    'value': 'IsMonth'
  }
  ]

  constructor(public route: ActivatedRoute,
    private dashboardService: DashboardService,
    private utilService: UtilService,
    private ngxLoader: NgxUiLoaderService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private render: Renderer,
    private router: Router,
    private formBuilder: FormBuilder, private profileService: ProfileService, ) {

  }

  ngOnInit() {

    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    if (loginLocalStorage != null && loginLocalStorage != undefined) {
      this.Loginchecker = true;
      this.loginId = loginLocalStorage.Id;
      this.userType = loginLocalStorage.UserType;
    } else if (registerUserData != null && registerUserData != undefined) {
      this.Loginchecker = true;
      this.loginId = registerUserData.Id;
      this.userType = registerUserData.UserType;
    }

    window.scrollTo(0, 0);

    this.advanceSearchData('');
    this.showCountry();
    this.slider();
    this.getLocationByIpAddress();
    this.getSystemIp();
  }

  getSystemIp() {
    this.dashboardService.getSystemIp().subscribe((res) => {
      if (res != null) {
        this.ipAddress = res['ip'];

      }
    }, error => {
      this.handleError(error);
    })
  }
  getLocationByIpAddress() {

    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.dashboardService.getGEOLocation(this.latitude, this.longitude).subscribe((response) => {
          console.log('response===address ip ', response['results'].length);

          for (var i = 0; i < response['results'].length; i++) {
            var addr = response['results'][i];
            console.log('addr.types[0]======', addr.address_components[0], '---', addr.types[0])
            switch (addr.types[0]) {
              case 'locality': this.selectedCity = addr.address_components[0].long_name;
                break;
              case 'administrative_area_level_1': this.selectedState = addr.address_components[0].long_name;
                break;
              case 'country': this.selectedCountry = this.latLongCountry = addr.address_components[0].short_name;
                break;
              case 'postal_code': //this.getPostalCode = addr.address_components[0].long_name;;
                break;
              default: 'default block';
            }

            if (this.selectedCountry != '' && this.selectedCountry != undefined) {
              this.showCountry();
              this.allEvents(this.eventType, this.selectedCountry, this.eventStatus, this.dateFilterRadioSelected, this.ratingNumber);
            }
            // if(this.selectedCity != '' && this.selectedCity != undefined){              
            //   this.onSearchChangeCountry({'term':this.selectedCity})
            // }
            // if (this.selectedState != '' && this.selectedState != undefined) {
            // // let event = {'CountryCode' : this.selectedState};
            //   this.onChangedCountryStateCity(this.selectedState);
            // }
          }
        }, error => {
          this.handleError(error);
        })
      });
    }
  }
  slider() {
  }

  /**
   * @author: smita
   * @function use: showCountry function use for country list show on search drop-down 
   * @date : 17-12-2019
   */
  showCountry() {

    this.profileService.CountryList().subscribe((data) => {
      if (data.length > 0) {
        // this.ngxLoader.stop();
        // console.log('country===', data);
        this.countriesData = data;
        data.filter(element => {
          if (element.CountryCode != null && element.CountryCode == this.selectedCountry) {
            // this.countriesData = element;
            this.selectedCity = element.CountryCode;
          }
        });
      }
    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
    })
  }

  /**
  * @author: smita
  * @function use: onSearchChangeCountry function use for pass search key and get country , state, city data
  * @date : 17-12-2019
  */
  onSearchChangeCountry(event) {
    if (event.term.length > 0 && event.term.length >= 3) {
      this.dashboardService.searchEventByCountry(event.term).subscribe((data: any[]) => {
        if (data.length > 0) {
          //existing array filter 
          // var list = data.filter(o =>
          //   Object.keys(o).some(k => o.CountryName.toLowerCase().includes(event.term.toLowerCase())
          //   ));          
          this.countriesData = data;
        }
      }, error => {
        this.handleError(error);
      });

    }
  }

  /**
   * @author: smita
   * @function use: onChangedCountryStateCity function use for show events by selected country,state,city
   * @date : 17-12-2019
   */

  onChangedCountryStateCity(event) {
    if (event != undefined && event.CountryCode != undefined && event.CountryCode != '') {
      // this.dashboardSearch = event.CountryCode;
      this.selectedCountry = event.CountryCode;
      this.allEvents(this.eventType, event.CountryCode, this.eventStatus, this.dateFilterRadioSelected, this.ratingNumber);
    } else {
      if (this.selectedCity == null) {
        this.selectedCountry = this.latLongCountry;
        this.allEvents(this.eventType, this.selectedCountry, this.eventStatus, this.dateFilterRadioSelected, this.ratingNumber);
        this.showCountry();
      }
    }
  }


  /**
  * @author: smita
  * @function use: advanceSearchData function use for show artist, organizer and events list in autocomplete text field
  * @date : 
  */

  advanceSearchData(searchKey) {

    let advSearchKey = (searchKey != '' && searchKey != undefined) ? searchKey : '';
    this.dashboardService.advanceSearchContent(advSearchKey)
      .subscribe((data: any[]) => {
        this.errorMessage = '';
        if (data.length > 0) {
          this.advSearchResponse = data;
        }
      }, error => {
        this.handleError(error);
      });
  }

  /**
   * @author: smita
   * @function use: eventClick function use for click on event redirect to event details screen
   * @date : 
   */
  eventClick(eventId) {

    if (this.eventType == 'pastEvents') {
      this.router.navigate(['/event-details', btoa(eventId), btoa('true')]);
    } else {
      this.router.navigate(['/event-details', btoa(eventId), btoa('false')]);
    }
    //this.eventType == 'pastEvents'? let IsPast = true: 

  }

  /**
  * @author: smita
  * @function use: "QuickEventSearch" function use for As per selected value search data and redirect to respective screen,
  * like - 'select organizer on autocomplete, redirect to organizer created events list'
  * @date : 
  */

  quickEventSearch(event) {

    if (event.Group == 'Artist') {
      this.router.navigate(['/artist-events', btoa(event.Id)]);// send artist id 
    } else if (event.Group == 'Organizer') {
      this.router.navigate(['/organizer-events/', btoa(event.Id)]) //send organizer id
    } else if (event.Group == 'Skill') {
      this.router.navigate(['/skills-events/', btoa(event.Id)]) //send skill id
    } else {
      this.router.navigate(['/event-details/', btoa(event.Id), btoa('false')]) //send event id and false use for event is public
    }
  }

  /**
  * @author: smita
  * @function use: "onAdvanceSearchChange" function use for pass search key and filter data
  * @date : 15-1-2020
  */

  onAdvanceSearchChange(event) {
    if (event.term != '' && event.term != undefined && event.term.length >= 3) {
      this.advanceSearchData(event.term); //function call
      this.advanceSearchKey = event.term;
    }
  }

  /**
   * @author :smita
   * @function Use :  onDateFilterItemChange function use for date filter change value get and show as per events
   * @Date : 17-1-2020 
   * @param dateFilterRadioSelected: selected item 
   */

  onDateFilterItemChange(item) {
    if (item.value != '' && item.value != undefined) {
      this.dateFilterRadioSelected = item.value;
      this.ratingNumber = 0;
      this.allEvents(this.eventType, this.selectedCountry, this.eventStatus, this.dateFilterRadioSelected, this.ratingNumber);
    }
  }

  /**
   * @author : smita
   * @function use : "allEvent function use for show up coming events"
   * @date : 14-10-2019
   */
  allEvents(eventType, searchText, eventSecurityStatus, isDateFilter, RatingNumber) {
    let dateFilterPram = (isDateFilter != '' && isDateFilter != undefined) ? isDateFilter : this.dateFilterRadioSelected;
    if (eventType == 'upComing' && eventSecurityStatus == true) {
      this.eventStatus = eventSecurityStatus;     // private tab
    } else {
      this.eventStatus = eventSecurityStatus; // upcoming tab & past event tab click
    }
    this.eventType = eventType;
    let searchTextValue = (searchText != null) ? searchText : null;
    let searchData = (searchTextValue == null) ? null : searchTextValue.trim();

    let eventPostObject = {
      "SearchTitle": searchData,
      "EventAttribute": this.eventType,
      "UserId": null,
      "IsPrivate": this.eventStatus,
      "DateFilter": dateFilterPram,
      "Type": "Event",
      "rating": RatingNumber
    }

    this.dashboardService.allEventContent(eventPostObject)
      .subscribe((data: any[]) => {
        this.ngxLoader.stop();
        this.errorMessage = '';
        if (data.length > 0) {
          this.eventsResponse = data;
          this.eventDataLength = data.length;
          console.log("len", this.eventsResponse)
          this.eventTabName = (this.eventType == 'upComing') ? 'upcomingEvent' : 'pendingEvent';
        }
      }, error => {
        this.eventsResponse = [];
        this.eventDataLength = '';
        this.ngxLoader.stop();
        // this.handleError(error);
      });
  }

  searchEventByRating(rating_no) {
    console.log(rating_no);
    let ratingArray = rating_no.split(" ");
    this.ratingNumber = ratingArray[0];
    this.allEvents(this.eventType, this.selectedCountry, this.eventStatus, this.dateFilterRadioSelected, this.ratingNumber);
    console.log(ratingArray[0]);
  }

  shareModalClick() {
    $("#mySocialShareModal").modal("show");
  }

  handleError(error) {

    if (error.error instanceof ErrorEvent) {
      // client-side error
      this.errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      console.log(error);
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      this.errorMessage = this.ShowErrorMsg;// `${error.error.message}`;
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      });
    }
    // window.alert(errorMessage);
    // return throwError(errorMessage);
  }
}