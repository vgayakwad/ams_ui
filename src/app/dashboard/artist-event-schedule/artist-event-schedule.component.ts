import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { OptionsInput } from '@fullcalendar/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import * as moment from 'moment';
import { DashboardService } from '../../services/dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { UtilService } from '../../services/util.service';
import { Observable, throwError } from 'rxjs';
import { ProfileService } from '../../services/profile.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { HttpClient  } from '@angular/common/http';
declare var $: any;

@Component({
  selector: 'app-artist-event-schedule',
  templateUrl: './artist-event-schedule.component.html',
  styleUrls: ['./artist-event-schedule.component.css'],
  providers: [DashboardService, UtilService, ProfileService]
})
export class ArtistEventScheduleComponent implements OnInit {

  options: OptionsInput;
  eventDetails: any;
  artistPastEventDetails: any;
  artistPastEventDetailsLength:any;
  artistId: any;
  eventSchedule: any;
  ShowErrorMsg: any;
  artistName: any;
  artistCatogory: any;
  p: number = 1;
  eventErrorMessage: any;
  profileImageURL: any;
  eventDataLength: any = 0;
  aboutMe: any;
  userType: any;
  artistCategoryList: any;
  // artistCategoryName:any;
  localStorageLogin: any;
  localStorageRegister: any;
  loginId: any;
  selectedArtistCategoryId = [] = [];
  selectedDateFilter: any = "ISMONTH";
  searchEventName: any = "";
  artistRating: any = 0; 
  artistRatingErrorMessage: any = '';
  artistAverageRating:any;
  usersRating:any;
  ratingNumber: any;
  ipAddress:any;
  //static date filter
  dateFilter: any = [{
    name: 'This Week',
    value: 'ISWEEK'
  },
  {
    name: 'Today',
    value: 'IsToday'
  },
  {
    name: 'Tomorrow',
    value: 'IsTomorrow'
  },
  {
    name: 'This Weekend',
    value: 'IsWeekend'
  },
  {
    name: 'This Month',
    value: 'ISMONTH'
  }
  ]


  // @ViewChild('fullcalendar', {static: false}) fullcalendar: FullCalendarComponent

  constructor(private dashboardService: DashboardService, private route: ActivatedRoute, private ngxLoader: NgxUiLoaderService,
    private router: Router, private utilService: UtilService, private profileService: ProfileService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private http:HttpClient) {
    this.artistId = atob(this.route.snapshot.paramMap.get('paramA'));
  }



  ngOnInit() {
    this.ngxLoader.start();   
    window.scrollTo(0, 0);
    this.getArtistEventDetails();
    this.getArtistPerformedEvents();
    this.getSystemIp();
    // this.getArtistCategoryList();
    this.localStorageLogin = this.storage.get("login");
    this.localStorageRegister = this.storage.get("register");
    if (this.localStorageLogin != null) {
      this.loginId = this.localStorageLogin.Id;
    }
    if (this.localStorageRegister != null) {
      this.loginId = this.localStorageRegister.Id;
    }
  }
  /**
 * @author : Snehal
 * @param element
 * @function use : "onClick on check box set focus to artist event list"
 * @date : 23-01-2020
 */
  navigateToSection(element): void {
    if (this.loginId != this.artistId) {
      $('#' + element)[0].scrollIntoView();
    }
  }
  //end

  /**
* @author : Snehal
* @param selectedDateFilterValue
* @function use : "search the events on selection of date filter(eg:today,tomorrow)"
* @date : 17-01-2020
*/
  onSelectDateFilter(selectedDateFilterValue) {
    console.log(selectedDateFilterValue);
    this.selectedDateFilter = selectedDateFilterValue;
    this.ratingNumber = 0;
    this.getArtistEventDetails();
    this.getArtistPerformedEvents();

  }
  //end

  /**
 * @author : Snehal
 * @param e
 * @param artistCategoryId
 * @function use : "search the events on selection of artist category"
 * @date : 17-01-2020
 */
  onSelectArtistCategory(e, artistCategoryId) {
    console.log(artistCategoryId);
    console.log(e.target.checked);
    if (e.target.checked) {
      this.selectedArtistCategoryId.push(artistCategoryId);
      this.getArtistEventDetails();
      this.getArtistPerformedEvents();
    } else {
      let removeIndex = this.selectedArtistCategoryId.findIndex(item => item === artistCategoryId);
      if (removeIndex !== -1) {
        this.selectedArtistCategoryId.splice(removeIndex, 1);
      }
      this.getArtistEventDetails();
      this.getArtistPerformedEvents();
    }
    console.log("final", this.selectedArtistCategoryId);
  }
  //end

  /**
 * @author : Snehal
 * @param selectedEventName
 * @function use : "get selected search value of organizer from search-bar component"
 * @date : 16-01-2020.
 */
  getSearchUserList(selectedEventName) {
    console.log("selected.....", selectedEventName);
    this.searchEventName = selectedEventName.trim();
    this.getArtistEventDetails();
    this.getArtistPerformedEvents();
  }
  //end

  /**
   * @author : Snehal
   * @function use : "get artist Category List"
   * @date : 14-01-2020
   */
  // getArtistCategoryList(){
  //   this.ngxLoader.start();
  //   this.profileService.artistCategoryList().subscribe((data) => {
  //     if (data.length > 0) {
  //       this.ngxLoader.stop();
  //       this.artistCategoryList = data;
  //     }
  //   }, error => {
  //     this.ngxLoader.stop();
  //     this.handleError(error);
  //   });
  // }


  /**
   * @author : Snehal
   * @function use : "get artist all information about events,personal details"
   * @date : 14-01-2020
   */
  getArtistEventDetails() {
     this.ngxLoader.start();
    this.eventDetails = [];
    this.dashboardService.getArtistEventById(this.searchEventName, this.selectedArtistCategoryId, this.selectedDateFilter, this.artistId).subscribe(res => {
      this.ngxLoader.stop();
      console.log("artist event", res);
      if (Object.keys(res).length != 0) {
        this.artistName = (res.FirstName != '' && res.LastName != '') ? res.FirstName + ' ' + res.LastName : '';
        this.artistCatogory = res.ArtistCategoryName;
        this.profileImageURL = (res.ProfileImageURL != null) ? res.ProfileImageURL : null;
        this.userType = (res.UserType != null) ? res.UserType : null;
        this.aboutMe = res.Description;
        this.artistAverageRating = (res.AverageRating!=null)?res:0;
        this.usersRating = (res.CustomerRating!=null)?res.CustomerRating:0;
        if (this.selectedArtistCategoryId.length == 0) {
          this.artistCategoryList = res.ArtistCategoryList;
        }
        if (res.EventList != null) {
          this.eventDetails = res.EventList;
          this.eventDataLength = res.EventList.length;
        }
        else {
          this.eventDataLength = 0
        }
      }
      else {
        this.eventDataLength = 0
      }
    }, error => {
      this.eventDetails = [];
      this.ngxLoader.stop();
      this.eventErrorMessage = error.error;
      // this.handleError(error);
    });
  }
  //end

  /**
   * @author: Smita
   * @function use : getArtistPerformedEvents function use for artist performed events 
   * @date : 19-2-2020
   */
  getArtistPerformedEvents() {

    let options = {
      SearchTitle: this.searchEventName,
      CategoryId: this.selectedArtistCategoryId,
      //EventAttribute: "PASTEVENTS",
      IsPrivate: false,
      DateFormat: this.selectedDateFilter,
      UserId: this.artistId,
     // Type: "Event",
      rating: this.ratingNumber,
      UserType:"Artist"
    };
      this.ngxLoader.start();
    this.dashboardService.getArtistOrganizerPastEvents(options).subscribe((data) => {
      if (Object.keys(data).length != 0) {
        this.ngxLoader.stop();
        console.log('passs artist eventssssssss', data);
        this.artistPastEventDetails = data;
        this.artistPastEventDetailsLength = data.length;
      }else{
        this.ngxLoader.stop();
        this.artistPastEventDetailsLength =0;
      }
    }, error => {
      this.artistPastEventDetails = [];
      this.artistPastEventDetailsLength =0;
      this.ngxLoader.stop();
      this.eventErrorMessage = error.error;
    })
  }


  /**
   * @author : Snehal
   * @param eventId
   * @function use : "navigate to show event details"
   * @date : 13-01-2020
   */
  screenRedirect(eventId, eventType) {
    if(eventType == 'upComing'){
      this.router.navigate(['/event-details', btoa(eventId), btoa('false')]);
    }else{
      this.router.navigate(['/event-details', btoa(eventId), btoa('true')]);
    }
    
  }
  //end


  /**
   * @author : Smita 
   * @function Use : "on artist Rate function use for get no-of start selected user"
   * @param event 
   */
  onArtistRate(event) {
    this.artistRating = event.newValue;
  }

  /**
   * @Author :smita
   * @function use : get system Ip
   * @date : 25-2-2020
   */
  getSystemIp(){      
    this.dashboardService.getSystemIp().subscribe((res) => {
      if(res!=null){
        this.ipAddress=res['ip'];
      }           
    }, error => {   
      this.handleError(error);
    })
   }

  /**
  * @author : Smita 
  * @function Use : "submitArtistRating  function use for artist rating save in database"
  * @param event 
  */
  submitArtistRating() {

    if (this.artistRating != 0 && this.artistRating != undefined) {
      this.artistRatingErrorMessage = '';

      let addRatingObject = {
        "EventOrUserId": this.artistId,
        "Rating": this.artistRating,
        "Type": 'Artist',
        "IPAddress":this.ipAddress
      }
      this.dashboardService.addRating(addRatingObject).subscribe((data) => {
        console.log('data==', data);
        if (data != null) {
          this.ratingNumber = 0;
          $("#myArtistModal").modal("hide");
          this.getArtistEventDetails();
          this.getArtistPerformedEvents();
          this.artistRating = 0;
          const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 3000
          })
          Toast.fire({
            icon: 'success',
            title: 'Add Artist Rating Successfully.'
          });        

        }
      }, error => {
        this.handleError(error);
      })

    } else {
      this.artistRatingErrorMessage = 'Please select at least 1 star';
    }
  }

  /**
   * @Author: smita
   * @param rating_no 
   * @date : '25-2-2020
   * @function use: past event search by star rating number
   */

  searchEventByRating(rating_no){   
    let ratingArray = rating_no.split(" ");
    this.ratingNumber = ratingArray[0];
    this.getArtistPerformedEvents();   
  }

  /**
  * @author : Snehal
  * @param error
  * @function use : "Client and Server side Error handling "
  * @date : 13-11-2019
  */
  handleError(error) {
    let errorMessage = '';
    this.ShowErrorMsg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      });
    }
    return throwError(errorMessage);
  }
  //end
}




