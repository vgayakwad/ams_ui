import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistEventScheduleComponent } from './artist-event-schedule.component';

describe('ArtistEventScheduleComponent', () => {
  let component: ArtistEventScheduleComponent;
  let fixture: ComponentFixture<ArtistEventScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistEventScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistEventScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
