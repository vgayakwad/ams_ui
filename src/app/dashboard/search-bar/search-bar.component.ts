import { Component, OnInit,EventEmitter ,Output,Input,Inject} from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  userType:any;

  constructor( @Inject(LOCAL_STORAGE) private storage: StorageService,
  private ngxLoader: NgxUiLoaderService,private dashboardService: DashboardService) { }

  @Output() searchedUser: EventEmitter<any> = new EventEmitter();
 

  ngOnInit() {

    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    if (loginLocalStorage != null && loginLocalStorage != undefined) {     
      //this.loginId = loginLocalStorage.Id;
      this.userType = loginLocalStorage.UserType;
    } else if (registerUserData != null && registerUserData != undefined) {     
     // this.loginId = registerUserData.Id;
      this.userType = registerUserData.UserType;
    } 
  }

 /**
   * @author : Snehal
   * @function use : "get user search key and emit that search key as output"
   * @date : 15-01-2020
   */
  searchUserKey(searchUserName){
    console.log(searchUserName);
    this.searchedUser.emit(searchUserName);
  }
  //end
}
