import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfileService } from '../../services/profile.service';
import { DashboardService } from '../../services/dashboard.service';
import { UtilService } from '../../services/util.service';
import { Observable, throwError } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { formatDate } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, Appearance } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  providers: [DashboardService, UtilService, ProfileService],
})

export class EventsComponent implements OnInit {
  eventsAddForm: FormGroup;
  submitted = false;
  errors: any = {};
  galleryErrorsObj: any = {};
  galleryError: any = {
    isErrorGalleryFlag: false,
    galleryErrorMessage: ''
  };
  error: any = {
    isError: false,
    errorMessage: '',
    isErrorArtistName: false,
    errorMessageArtistName: '',
    isErrorEventName: false,
    errorMessageEventName: '',
    isErrorContactFlag: false,
    errorMessageContactNo: '',
    isErrorArtistAndCategoryFlag: false,
    errorMessageArtistAndCategory: '',
    isErrorArtistCategoryFlag: false,
    errorMessageArtistCategory: '',
    isErrorCoverPhotoFlag: false,
    errorMessageCoverPhoto: ''
  };

  ShowErrorMsg: any;
  artist_grid_flag = false;
  allArtistList:any;
  artistId:any;
  artistCategoryList:any;
 // artistCategoryData: any;
  eventsCategoryData: any;
  addedArtistListData: any;
  //category_name: any = [];
  dataRefresher: any;
  allCountryData: any;
  allStateData: any;
  allTimeZone: any;
  allCityData: any;
  allArtistData: any;
  artistProfileImage: any;
  updatedAddress: any;
  keyword = 'Name';
  selected_artist_name: any;
  fileData: File = null;
  base64textString: any = '';
  imgEventURL: any;
  city_id: any;
  public appearance = Appearance;
  public zoom: number;
  public latitude: number;
  public longitude: number;
  public selectedAddress: PlaceResult;
  getCountry :any;
  getState: any = null;
  getStateLongName:any;
  getCity: any;
  getTimeZoneOffset: any;
  timeZoneId: any = null;
  getPostalCode: any;
  markers: [{}];
  previewdata: any;
  gallaryEventsUrls = [];
  editEventId: any;
  eventPopulateData: any;
  eventTypeId: any = null;
  artist_category_nameArr = [];
  galleryObj = {}
  deleteObj = {};
  isShown: boolean = false;
  galleryPhotos: any;
  galleryPhotosLength: any;
  eventImageUpdateFlag = false;
  venuePlaceholder = 'Venue details';
  galleryImgErrorMessage: any;
  isPastEvent:any;
  //artistAvailableFromdate: any;
  //artistAvailableTodate: any;
  isPrivateValue: boolean;
 // scheduleId: any;
  loginId:any;
  public minDate = moment(new Date()).format("YYYY-MM-DD");

  constructor(private profileService: ProfileService,
    private formBuilder: FormBuilder,
    private dashboardService: DashboardService,
    private router: Router,
    private route: ActivatedRoute,
    private utilService: UtilService,
    private ngxLoader: NgxUiLoaderService,
    @Inject(LOCAL_STORAGE) private storage: StorageService) {

    if (this.eventsAddForm == undefined) {
      this.storage.remove('artist_info');
      this.storage.remove('artist_GridData');
    }
    this.editEventId = atob(this.route.snapshot.paramMap.get('paramA'));
    this.isPastEvent = atob(this.route.snapshot.paramMap.get('paramB'));
  }

  ngOnInit() {
    this.stepper();
    this.ngxLoader.start();
    this.zoom = 10;
    //this.latitude = 47.608013;
   // this.longitude = -122.335167;
    window.scrollTo(0, 0);

    this.getArtistList();
    //this.getArtistCategory();
    this.showAddedArtist(null, null, null);
    this.getCountryList();   
    this.getEventsCategList();
    this.getGalleryPhotos();
    this.getPopulateEventInfo();
    

    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    if (loginLocalStorage != null && loginLocalStorage != undefined) {    
      this.loginId = loginLocalStorage.Id;     
    } else if (registerUserData != null && registerUserData != undefined) {     
      this.loginId = registerUserData.Id;
     
    }


    this.eventsAddForm = this.formBuilder.group({
      event_name: ['', [Validators.required, Validators.maxLength(150)]],
      from_date_time: ['', Validators.required],
      to_date_time: ['', Validators.required],
      venue_details: [''],
      event_description: ['', Validators.required],
      event_category: ['', Validators.required],
      artist_name: [''],
      artist_category: [''],
      country: [''],
      state: [''],
      city: [''],
      timeZone: [''],
      IsPrivate: ['', Validators.required]  //event type public or private
    }, {

    });
    this.setCurrentPosition();

  }


  stepper() {
    $('[data-toggle="tooltip"]').tooltip();    
    var navListItems = $('div.setup-panel div a'),
      allWells = $('.setup-content'),
      allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
        $item = $(this);

      if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-primary').addClass('btn-secondary');
        $item.addClass('btn-primary');
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
      }
    });

    allNextBtn.click(function () {
      var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        curInputs = curStep.find("input[type='text'],input[type='url']"),
        isValid = true;

      $(".form-group").removeClass("has-error");
      for (var i = 0; i < curInputs.length; i++) {
        if (!curInputs[i].validity.valid) {
          isValid = false;
          $(curInputs[i]).closest(".form-group").addClass("has-error");
        }
      }

      if (isValid)
        nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
  }


  private setCurrentPosition() {
    this.zoom = 12;
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
          this.dashboardService.getGEOLocation(this.latitude,this.longitude).subscribe((response) => {
                       
            for (var i = 0; i < response['results'].length; i++) {
              var addr = response['results'][i];              
              switch (addr.types[0]) {
                case 'locality':  this.getCity =addr.address_components[0].long_name;
                  break;
                case 'administrative_area_level_1': this.getState = addr.address_components[0].short_name;
                                                    this.getStateLongName = addr.long_name;
                  break;
                case 'country': this.getCountry =  addr.address_components[0].short_name;
                  break;
                case 'postal_code': //this.getPostalCode = addr.address_components[0].long_name;;
                  break;
                default: 'default block';
              } 
            }              
          }, error => {
            this.handleError(error);
          })
        });     
      }    
  }

  addMarker(lat: number, lng: number) {
    this.latitude = lat;
    this.longitude = lng;
    this.zoom = 12;
    let geocoder = new google.maps.Geocoder;
    let latlng = { lat: this.latitude, lng: this.longitude };
    let safe = this;
    geocoder.geocode({ 'location': latlng }, function (results) {
      if (results[0]) {
        safe.zoom = 12;
      } else {
        console.log('No results found');
      }
    }); //geocode end
  }

  onAutocompleteSelected(result: PlaceResult) {
    console.log('------------',result);
    if (result.address_components != null) {

      this.eventsAddForm.controls['venue_details'].setValue(result.formatted_address);
      //address offset set in getTimeZoneOffset veriable and call timeZone api for get timeZone id
      // if (result.utc_offset != null && result.utc_offset != undefined) {
      //   this.getTimeZoneOffset = result.utc_offset;
      //   this.getTimeZone(this.getCountry)
      // }
      for (var i = 0; i < result.address_components.length; i++) {
        var addr = result.address_components[i];
        switch (addr.types[0]) {
          case 'locality': this.getCity = addr.long_name;
            break;
          case 'administrative_area_level_1': this.getState = addr.short_name;
            this.getStateLongName = addr.long_name;
            break;
          case 'country': this.getCountry = addr.short_name;
            break;
          case 'postal_code': this.getPostalCode = addr.long_name;
            break;
          default:
          //default block statement;
        }
        
        if (this.getCountry != '' && this.getCountry != undefined) {          
         // this.getTimeZone(this.getCountry);        
          this.countrySelectChangeHandler(this.getCountry);
        }
        if (this.getState != '' && this.getState != undefined && this.getCountry == 'IN') {         
          this.stateSelectChangeHandler(this.getStateLongName);
        }else if(this.getState != '' && this.getState != undefined && this.getCountry != 'IN'){
          this.stateSelectChangeHandler(this.getState);
        }
      }

    } else {
      console.log('Address not found');
    }
    //alert(this.eventsAddForm.controls['from_date_time'].value.getTimezoneOffset())
    console.log('timeeeeeeeeeee',console.log(Intl.DateTimeFormat().resolvedOptions().timeZone));
  }

  onLocationSelected(location: Location) {
    console.log('onLocationSelected: ', location);
    this.latitude = location.latitude;
    this.longitude = location.longitude;
    // this.zoom = 12;
  }

  get f() { return this.eventsAddForm.controls; }

  /**
   * @author: smita
   * @function use : getArtistList use for get all artist and show on autocomplete text box
   * @date : 20-1-2020
   */
  getArtistList(){
    this.dashboardService.getSearchUser('','Artist').subscribe((data)=>{
      
      if(data!=undefined && data.length>0){
        // filter use on artist list for get artist name and add new "artist_fullName parameter in existing object"
      data.filter(element => {        
        element['artist_fullName'] = element.FirstName+' '+element.LastName;       
      });
        this.allArtistList = data;
        console.log('artist list===',  this.allArtistList);
      }else{
        this.allArtistList = [];
        this.errors['isErrorArtistAndCategoryFlag'] = true;
        this.errors['errorMessageArtistAndCategory'] = 'No any artists.'
        this.error = this.errors;
      }
      
    }, error =>{
      this.handleError(error);
    })
  }

  /**
   * @author: smita
   * @param filterKey 
   * @function use : artistFilter function use for if user enter value in artist autocomplete filter data
   * @date : 20-1-2020
   */

  artistFilter(filterKey){
    console.log('filterKey==',filterKey);
  }

  /**
   * @author: smita
   * @param artist 
   * @function use : onArtistChange function use for if user select any artist in autocomplete get artist id and call artistCategory
   * @date : 20-1-2020
   */
  onArtistChange(artist){
    console.log('artist==',artist);

   // this.selectedPeople = [];
    if(Object.keys(artist).length > 0){
      this.artistId = artist.Id;
      this.artist_category_nameArr = [];
      this.dashboardService.getArtistCategoryByArtistId(this.artistId).subscribe((res) =>{
        if(res!=undefined && Object.keys(res).length > 0){
          this.artistCategoryList = res;
          console.log('cateogry===',res);
        }
      },error =>{
        this.artistCategoryList =[];
      //  this.handleError(error);
      })
    }else{
          this.errors['isErrorArtistAndCategoryFlag'] = true;
          this.errors['errorMessageArtistAndCategory'] = 'No any category for this artist, please select another artist.'
          this.error = this.errors;
    }
  }

  /*
   *@ author :Smita 
   *@ function use : getArtistCategory function use for get all Artist category
   *@ 21-10-2019 
   */
  // getArtistCategory() {
  //   this.ngxLoader.start();
  //   this.profileService.artistCategoryList().subscribe((data) => {
  //     if (data.length > 0) {
  //       this.ngxLoader.stop();
  //      // this.artistCategoryData = data;
  //     }
  //   }, error => {
  //     this.ngxLoader.stop();
  //     this.handleError(error);
  //   });

  // }

  /*
  *@author : smita
  *@function use : artistCategoryChangeHandler use for get Artist list by category id,event_From_date, event_to_date
  *@date: Updated on 12-12-2019
  */
  // artistCategoryChangeHandler(artist_category_id) {

  //   console.log('555555555555222222222');
  //   if (artist_category_id != '' && artist_category_id != undefined && this.eventsAddForm.value.from_date_time != '' && this.eventsAddForm.value.from_date_time != undefined
  //     && this.eventsAddForm.value.to_date_time != '' && this.eventsAddForm.value.to_date_time != undefined) {
  //     let from_date = moment(this.eventsAddForm.value.from_date_time).format('YYYY-MM-DD hh:mm:ss');
  //     let to_date = moment(this.eventsAddForm.value.to_date_time).format('YYYY-MM-DD hh:mm:ss');

  //     this.dashboardService.GetArtistList(artist_category_id, from_date, to_date).subscribe((data) => {
  //       console.log('artist data-------', data);
  //       if (Object.keys(data).length > 0) {
  //         this.errors['isErrorArtistAndCategoryFlag'] = false;
  //         this.allArtistData = data;
  //       } else {

  //         console.log('77', this.eventsAddForm.controls['artist_name'].value)
  //         this.allArtistData = [];
  //         this.errors['isErrorArtistAndCategoryFlag'] = true;
  //         this.errors['errorMessageArtistAndCategory'] = 'No any artist present for this category, please select another category.'
  //         this.error = this.errors;
  //         this.eventsAddForm.controls['artist_name'].setValue('');
  //         console.log('9999', this.eventsAddForm.controls['artist_name'].value)
  //       }

  //     }, error => {
  //       this.handleError(error);
  //     })

  //   }else{
  //     this.errors['isErrorArtistAndCategoryFlag'] = true;
  //     this.errors['errorMessageArtistAndCategory'] = 'No any artist present for this category, please select another category.'
  //     this.error = this.errors;
  //   }


  // }

  /*
   *@ author :Smita 
   *@ function use : getEventsList function use for get all Events category name
   *@ 21-10-2019 
   */
  getEventsCategList() {
    this.ngxLoader.start();
    this.profileService.EventsCategoryList().subscribe((data) => {
      if (data.length > 0) {
        this.ngxLoader.stop();
        this.eventsCategoryData = data;
      }
    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
    });
  }

  removeArtistInGrid(artistUserId) {
    this.ngxLoader.start();
    let existingArtistList = this.storage.get("artist_GridData");
    const existingArtistInfo = this.storage.get("artist_info");
    // filter use on skip selected object in localStorage           
    let filterArtistData = existingArtistList.filter(data => data.ArtistUserId !== artistUserId);
    let filterArtistInfo = existingArtistInfo.filter(data => data.ArtistUserId !== artistUserId);
   //  let filterArtistInfo = existingArtistInfo.filter(data => data.ArtistUserId == artistUserId);

    if (filterArtistInfo != undefined && filterArtistInfo.length > 0 && filterArtistData != undefined && filterArtistData.length > 0) {

      // existingArtistInfo.filter(elementdata => {
      //   if (artistUserId == elementdata.ArtistUserId) {
      //     elementdata.IsRemoved = true;
      //     elementdata.Status = 'AVAILABLE'

      //   }
      // })
     // console.log('878788', existingArtistInfo);
      // if(artistUserId!=null && )
      this.storage.set('artist_info', filterArtistInfo);
      this.storage.set('artist_GridData', filterArtistData);
      this.ngxLoader.stop();
    } else {
      //single artist_info value set in localStorage
      // existingArtistInfo.filter(elementdata => {
      //   if (artistUserId == elementdata.ArtistUserId) {
      //     elementdata.IsRemoved = true;
      //     elementdata.Status = 'AVAILABLE';
      //   }
      // })
      // remove last single artist_GridData and artist_info in localStorage
      
     // this.storage.set('artist_info', existingArtistInfo);
      this.storage.remove('artist_info');
      this.storage.remove('artist_GridData');
    }

  }

  showAddedArtist(artistList, startDate, endDate) {
    this.ngxLoader.start();
    let existingArtistList = '';
    if (artistList != '' && artistList != undefined) {
      existingArtistList = artistList;
      const existingArtistData = [];
      const existingArtistInfo = [];
      let catIds = [];
      if (artistList != null && artistList != undefined) {

        for (let i = 0; i <= artistList.length; i++) {

          if(artistList != undefined && artistList[i]['ArtistCategories'] != null ){

            artistList[i]['ArtistCategories'].filter(res =>{
              console.log('dsfdsfsfsd',res)
              this.artist_category_nameArr.push(res.ArtistCategoryName);
              catIds.push(res.Id);
            })       
            
            
            }
          let data = {
            'Name': (artistList != null && artistList != undefined) ? artistList[i]['Name'] : null,
            'ArtistCategoryName': (this.artist_category_nameArr != null && this.artist_category_nameArr != undefined) ? this.artist_category_nameArr : '',
            'ProfileImageURL': (artistList != null && artistList != undefined) ? artistList[i]['ProfileImageURL'] : null,
            //'events_date': (startDate == endDate) ? moment(startDate).format('DD MMM')
            //  : (moment(startDate).format('DD MMM') + ' - ' + moment(endDate).format('DD MMM')),
            'ArtistUserId': (artistList != null && artistList != undefined) ? artistList[i]['id'] : null,
            'ArtistCategoryId': (catIds != null && catIds != undefined) ? catIds:null,
          }
          let info = {
            'ArtistUserId': (artistList != null && artistList != undefined) ? artistList[i]['id'] : null,
            'ArtistCategoryId': (catIds != null && catIds != undefined) ? catIds:null,
            //'Status': 'busy',
            //'IsRemoved': false,
            //'AvailableFrom': (artistList != null && artistList != undefined) ? moment(artistList[i]['AvailableFrom']).format('YYYY-MM-DD HH:mm:ss') : null,
            //'AvailableTo': (artistList != null && artistList != undefined) ? moment(artistList[i]['AvailableTo']).format('YYYY-MM-DD HH:mm:ss') : null,
           // 'ScheduleId': (artistList != null && artistList != undefined) ? artistList[i]['ScheduleId'] : null,
          }
          existingArtistInfo.push(info);
          existingArtistData.push(data);
          this.storage.set('artist_info', existingArtistInfo);
          this.storage.set("artist_GridData", existingArtistData);
          catIds =[];
          this.artist_category_nameArr= [];
        }
      }
    } else {
      this.storage.remove('artist_info');
      this.storage.remove('artist_GridData');
    }

    this.dataRefresher =
      setInterval(() => {
        this.ngxLoader.stop();
        let existingArtistList = this.storage.get("artist_GridData");
        if (existingArtistList != undefined && existingArtistList.length > 0) {
          this.artist_grid_flag = true;
          this.addedArtistListData = existingArtistList
        } else {
          //remove last artist in grid
          this.artist_grid_flag = false;
          this.addedArtistListData = [];
        }
        //Passing the false flag would prevent page reset to 1 and hinder user interaction
      }, 5000);


  }

  /*
   *@ author :Smita 
   *@ function use : user can added one more artist in event
   *@ 22-10-2019 
   */

  addArtistToEvent() {

    this.ngxLoader.start();

    this.selected_artist_name ='';
   // this.artist_category_nameArr = [];
    

    if (this.eventsAddForm.controls['artist_name'].value != '' && this.eventsAddForm.controls['artist_name'].value != undefined
      && this.eventsAddForm.controls['artist_category'].value.length>0 && this.eventsAddForm.controls['artist_category'].value != undefined) {

      this.errors['isErrorArtistAndCategoryFlag'] = false;     
      this.error = this.errors;   

      // filter use on artist category for get artist category name by using category id 
      this.artistCategoryList.filter(element => {
        let artistCategoryIds = this.eventsAddForm.controls['artist_category'].value; //array
        if(artistCategoryIds.length>0){
          for(let i=0;i<=artistCategoryIds.length; i++){
            if (element.Id == artistCategoryIds[i]) {          
              this.artist_category_nameArr.push(element.ArtistCategoryName);
              console.log('this.ArtistCategoryName===',this.artist_category_nameArr);
            }
          }
          console.log('this.category_name===',this.artist_category_nameArr);
        }
        
      });

      //allArtistList filter use for get  artist name, profileImage, artistAvailableDate, ArtistAvailableToDate by using artist id
      this.allArtistList.filter(elementdata => {
        if (elementdata.Id == this.eventsAddForm.controls['artist_name'].value) {        
          this.selected_artist_name = elementdata.FirstName+ ' '+elementdata.LastName;
          this.artistProfileImage = (elementdata.ProfileImageURL != null) ? elementdata.ProfileImageURL : null;
         // this.artistAvailableFromdate = elementdata.AvailableFrom;
        //  this.artistAvailableTodate = elementdata.AvailableTo;
         // this.scheduleId = elementdata.ScheduleId;
        }
      })

     // let from_date = moment(this.eventsAddForm.value.from_date_time).format('YYYY-MM-DD');
     // let to_date = moment(this.eventsAddForm.value.to_date_time).format('YYYY-MM-DD');

      if (this.artist_category_nameArr != undefined && this.artist_category_nameArr.length>0) {
        // get array of artist_GridData and artist_info from local storage
        const existingArtist = this.storage.get("artist_GridData") || []; //show on grid data 
        const existingArtistInfo = this.storage.get("artist_info") || []; //post data
        if (existingArtist.length == 0 && existingArtistInfo.length == 0) {
          // insert updated array to local storage      
          let data = {
            'Name': this.selected_artist_name,
            'ArtistCategoryName': this.artist_category_nameArr,
            'ProfileImageURL': this.artistProfileImage,            
            'ArtistUserId': this.eventsAddForm.controls['artist_name'].value,
            'ArtistCategoryId': this.eventsAddForm.controls['artist_category'].value,
          }
          let info = {
            'ArtistUserId': this.eventsAddForm.controls['artist_name'].value,
            'ArtistCategoryId': this.eventsAddForm.controls['artist_category'].value, //array
            //'Status': 'busy',
            //'IsRemoved': false,
           // 'AvailableFrom': moment(this.artistAvailableFromdate).format('YYYY-MM-DD HH:mm:ss'),
          //  'AvailableTo': moment(this.artistAvailableTodate).format('YYYY-MM-DD HH:mm:ss'),
            //'ScheduleId': this.scheduleId
          }
          existingArtistInfo.push(info); //api post data
          existingArtist.push(data); //grid show data

          this.storage.set('artist_info', existingArtistInfo);
          this.storage.set("artist_GridData", existingArtist);
         
           this.eventsAddForm.controls['artist_name'].setValue('');
           this.selected_artist_name ='';
           this.artist_category_nameArr = [];
           this.eventsAddForm.controls['artist_category'].setValue('');

          this.artist_grid_flag = true;
          this.ngxLoader.stop();
        } else {

          const existingArtist = this.storage.get("artist_GridData");
          console.log('000000000000000', existingArtist, this.allArtistData)

          existingArtist.filter(items => {
            let artistCategoryIds = this.eventsAddForm.controls['artist_category'].value;
            if (items.ArtistUserId == this.eventsAddForm.controls['artist_name'].value) {
              this.errors['isErrorArtistAndCategoryFlag'] = true;
              this.errors['errorMessageArtistAndCategory'] = 'Duplicate artist not allowed.'
              this.error = this.errors;

              this.eventsAddForm.controls['artist_name'].setValue('');             
              this.artist_category_nameArr = [];
              this.eventsAddForm.controls['artist_category'].setValue('');
            } 
             // this.errors['isErrorArtistAndCategoryFlag'] = true;
             // this.errors['errorMessageArtistAndCategory'] = 'No any category present for this artist, please select another artist.'
              //this.error = this.errors;              
            else {
              this.errors['isErrorArtistAndCategoryFlag'] = false;
              this.errors['errorMessageArtistAndCategory'] = '';
              this.error = this.errors;
              let data = {
                'Name': this.selected_artist_name,
                'ArtistCategoryName': this.artist_category_nameArr,
                'ProfileImageURL': this.artistProfileImage,
               // 'events_date': (from_date == to_date) ? moment(from_date).format('DD MMM')
               //   : (moment(from_date).format('DD MMM') + ' - ' + moment(to_date).format('DD MMM')),
                'ArtistUserId': this.eventsAddForm.controls['artist_name'].value,
                'ArtistCategoryId': this.eventsAddForm.controls['artist_category'].value,
              }
              let info = {
                'ArtistUserId': this.eventsAddForm.controls['artist_name'].value,
                'ArtistCategoryId': this.eventsAddForm.controls['artist_category'].value,
               // 'Status': 'busy',
               // 'IsRemoved': false,
                //'AvailableFrom': moment(this.artistAvailableFromdate).format('YYYY-MM-DD HH:mm:ss'),
               // 'AvailableTo': moment(this.artistAvailableTodate).format('YYYY-MM-DD HH:mm:ss'),
                //'ScheduleId': this.scheduleId
              }
              existingArtistInfo.push(info);
              this.storage.set('artist_info', existingArtistInfo);
              existingArtist.push(data);
              this.storage.set("artist_GridData", existingArtist);
              this.artist_grid_flag = true;
              this.OnAutoClear();
              this.eventsAddForm.controls['artist_name'].setValue('');
              this.selected_artist_name ='';
              this.artist_category_nameArr = [];
              this.eventsAddForm.controls['artist_category'].setValue('');

            }
          })
        }
      }

    } else {
      this.ngxLoader.stop();
      this.errors['isErrorArtistAndCategoryFlag'] = true;
      this.errors['errorMessageArtistAndCategory'] = 'Artist name and category is required.'
      this.error = this.errors;
    }
  }

  /*
  *@ author :Smita 
  *@ function use : getCountryList use for get all country data in api
  *@ 25-10-2019 
  */
  getCountryList() {
    this.ngxLoader.start();
    this.profileService.CountryList().subscribe((data) => {
      if (data.length > 0) {
        this.ngxLoader.stop();
        this.allCountryData = data;
      }

    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
    })
  }

  countrySelectChangeHandler(country_code) {
    this.getCountry = country_code;
    this.profileService.StateList(this.getCountry).subscribe((data) => {
      if (data.length > 0) {
        this.allStateData = data;
      }
    }, error => {
      this.handleError(error);
    })
  }

  // getTimeZone(country_code) {

  //   this.getCountry = country_code;
  //   this.profileService.getTimeZoneList('us').subscribe((data) => {
  //     if (data.length > 0) {
  //       data.filter(element => {
  //         if (element.UTC_OffSet == this.getTimeZoneOffset) {
  //           this.timeZoneId = element.Id;
  //         }
  //       })
  //       this.allTimeZone = data;
  //     }
  //   }, error => {
  //     this.handleError(error);
  //   });
  // }

  stateSelectChangeHandler(state_code) {

    this.getState = state_code;
    this.profileService.CityList(this.getState).subscribe((data) => {
      //console.log('city dataaaaaaaaaaaaaaa',data);
      data.filter(element => {        
        if (element.CityName == this.getCity) {
          this.city_id = element.Id;
        }
      });
      this.allCityData = data;
    }, error => {
      this.handleError(error);
    })
  }


  getPopulateEventInfo() {

    if (this.editEventId.trim() != 'new' && this.editEventId != undefined) {
      this.dashboardService.getEventDetails(this.editEventId,this.isPastEvent).subscribe((data) => {
        if (data != '' && data != undefined) {

          this.eventsAddForm.controls['event_name'].setValue((data.EventName != '') ? data.EventName : '');
          this.eventsAddForm.controls['event_description'].setValue((data.Description != '') ? data.Description : '');
          this.getCountry = (data.CountryCode != '' && data.CountryCode != undefined) ? data.CountryCode : '';
          this.getState = (data.StateCode != '' && data.StateCode != undefined) ? data.StateCode : '';
          this.getCity = (data.CityName != '' && data.CityName != undefined) ? data.CityName : '';
          this.eventTypeId = (data.EventTypeId != null && data.EventTypeId != undefined) ? data.EventTypeId : null;
          this.imgEventURL = (data.EventImageUrl != null && data.EventImageUrl != undefined) ? data.EventImageUrl : null;
          this.stateSelectChangeHandler(data.StateCode);
          this.eventsAddForm.controls['from_date_time'].setValue((data.EventStart != '') ? new Date(data.EventStart) : '');
          this.eventsAddForm.controls['to_date_time'].setValue((data.EventEnd != '') ? new Date(data.EventEnd) : '');
          this.isPrivateValue = (data.IsPrivate != null && data.IsPrivate != undefined) ? data.IsPrivate : '';
          // this.artistCategoryChangeHandler()
          // this.eventsAddForm.controls['from_date_time'].setValue((data.EventStart != '') ? moment(data.EventStart).format('YYYY-MM-DD') : '');
          // this.eventsAddForm.controls['to_date_time'].setValue((data.EventEnd != '') ? moment(data.EventEnd).format('YYYY-MM-DD') : '');
          this.timeZoneId = (data.TimeZoneId != null && data.TimeZoneId != undefined) ? data.TimeZoneId : '';
          if (data.Address != '' && data.Address != undefined) {
            this.isShown = !this.isShown;
            this.updatedAddress = data.Address;
          }
          this.showAddedArtist(data.ArtistsList, data.EventStart, data.EventEnd);

        } else {
          this.storage.remove('artist_info');
          this.storage.remove('artist_GridData');
        }

      }, error => {
        //this.ngxLoader.stop();
        this.handleError(error);
      })
    } else {
      this.storage.remove('artist_info');
      this.storage.remove('artist_GridData');
    }
  }

  showAddressBox() {
    this.isShown = false;
  }


  onAddEvents() {
    console.log('this.city_id',this.city_id);
    this.ngxLoader.start();
    const existingArtist = this.storage.get("artist_GridData");
    const artistInfo = this.storage.get("artist_info");
    this.submitted = true;
    let d2 = moment(this.eventsAddForm.controls['to_date_time'].value);
    let d1 = moment(this.eventsAddForm.controls['from_date_time'].value);    
    let hours = d2.diff(d1, 'hours');
 
    if (new Date(this.eventsAddForm.controls['to_date_time'].value) < new Date(this.eventsAddForm.controls['from_date_time'].value)) {
      this.errors['isError'] = true;
      this.errors['errorMessage'] = 'To Date must be greater than from date.' //To Date can\'t before form date
      this.error = this.errors;
    } else if(hours == 0){
      this.errors['isError'] = true;
      this.errors['errorMessage'] = 'Both date time are same.' //same date time
      this.error = this.errors;
    }else{     
      this.errors['isError'] = false;
      this.errors['errorMessage'] ="";
      this.error = this.errors;
    }

    // if ((this.eventsAddForm.controls['artist_name'].value == undefined || this.eventsAddForm.controls['artist_name'].value == '')
    //   && existingArtist == undefined) {
    //   this.errors['isErrorArtistName'] = true;
    //   this.errors['errorMessageArtistName'] = 'Artist name is required'
    //   this.error = this.errors;
    // } else {
    //   this.errors['isErrorArtistName'] = false;
    //   this.error = this.errors;
    // }


    // if ((this.eventsAddForm.controls['artist_category'].value == undefined || this.eventsAddForm.controls['artist_category'].value == '')
    //   && existingArtist == undefined) {
    //   this.errors['isErrorArtistCategoryFlag'] = true;
    //   this.errors['errorMessageArtistCategory'] = 'Artist category is required.'
    //   this.error = this.errors;
    // } else {
    //   this.errors['isErrorArtistCategoryFlag'] = false;
    //   this.error = this.errors;
    // }

    if (this.base64textString == '' && (this.imgEventURL == '' || this.imgEventURL == undefined)) {
      this.errors['isErrorCoverPhotoFlag'] = true;
      this.errors['errorMessageCoverPhoto'] = 'Event cover photo is required.'
      this.error = this.errors;
    } else {
      this.errors['isErrorCoverPhotoFlag'] = false;
      this.error = this.errors;
    }
    let checkErrorStatus = 0;
    for (var i in this.errors) {
      if (this.errors[i] == true) {
        checkErrorStatus = 1
      }
    }

    // && existingArtist != undefined && artistInfo != undefined
    if (this.eventsAddForm.valid && checkErrorStatus == 0) {

      let finalEventObject = {};
      if (this.editEventId != '' && this.editEventId != undefined && this.editEventId != 'new') {
        finalEventObject['EventId'] = this.editEventId;

      }
      let imageCoverUrl = (this.base64textString != '' && this.base64textString != undefined) ? this.base64textString : this.imgEventURL;
      if (this.base64textString != '' && this.base64textString != undefined) {
        this.eventImageUpdateFlag = true;
      }
      let eventAddress = (this.eventsAddForm.value.venue_details != '') ? this.eventsAddForm.value.venue_details : this.updatedAddress;

      finalEventObject['EventName'] = this.eventsAddForm.value.event_name.trim();
      finalEventObject['EventStartDate'] = moment(this.eventsAddForm.value.from_date_time).format('MM/DD/YYYY hh:mm a');
      //formatDate(this.eventsAddForm.value.from_date_time, 'MM/dd/yyyy hh:mm a', 'en-US', '+0530');
      finalEventObject['EventEndDate'] = moment(this.eventsAddForm.value.to_date_time).format('MM/DD/YYYY hh:mm a');
      finalEventObject['EventTypeId'] = this.eventsAddForm.value.event_category;
      finalEventObject['CountryCode'] = this.getCountry; //this.eventsAddForm.value.country;
      finalEventObject['StateCode'] = this.getState; //this.eventsAddForm.value.state;
      finalEventObject['CityId'] =  this.city_id; //this.eventsAddForm.value.city;
      finalEventObject['TimeZoneId'] = 0; //this.eventsAddForm.value.timeZone;
      finalEventObject['Address'] = eventAddress.trim();
      finalEventObject['Description'] = this.eventsAddForm.value.event_description.trim();
      finalEventObject['EventImage'] = imageCoverUrl;
      finalEventObject['IsEventImageUpdated'] = this.eventImageUpdateFlag;
      finalEventObject['ArtistsList'] = (artistInfo != '' && artistInfo != undefined) ? artistInfo : null;
      finalEventObject['IsPrivate'] = this.eventsAddForm.value.IsPrivate;

      this.dashboardService
        .addEvents(JSON.stringify(finalEventObject))
        .subscribe(result => {
          this.ngxLoader.stop();
          if (result != null) {
            const Toast = Swal.mixin({
              toast: true,
              showConfirmButton: false,
              timer: 3000
            })
            Toast.fire({
              icon: 'success',
              title: (this.editEventId != '' && this.editEventId != undefined && this.editEventId == 'new') ? 'Event Added Successfully.' : 'Event Updated Successfully.'
            });
            this.router.navigate(['/organizer-events/', btoa(this.loginId)])             
          }
        }, error => {
          this.ngxLoader.stop();
          this.handleError(error);
          // this.router.navigate(['/login']);     
        });
    } else {
      console.log('Event not inserted ');
    }
  }



  async fileProgress(fileInput: any) {
    //this.ngxLoader.start();
    try {
      const fileData = <File>fileInput.target.files[0];
      this.previewdata = await this.utilService.ImagePreview(fileData);
      let toArray = this.previewdata.split(",");
      this.base64textString = toArray[1] //this.previewdata.substr(23);

      if (this.base64textString == '') {
        this.errors['isErrorCoverPhotoFlag'] = true;
        this.errors['errorMessageCoverPhoto'] = 'Event cover photo is required.'
        this.error = this.errors;
      } else {
        this.errors['isErrorCoverPhotoFlag'] = false;
        this.error = this.errors;
      }
      this.imgEventURL = this.previewdata;
    } catch (error) {
      // this.ngxLoader.stop();
      this.handleError(error);
    }

  }

  /**
   * @author: Smita
   * @function Use: onSelectGalleyPhotos upload multi image and preview 
   * @date: 15-11-2019 
   */
  async  onSelectGalleyPhotos(event) {

    this.galleryErrorsObj['isErrorGalleryFlag'] = false;
    this.galleryError = this.galleryErrorsObj;

    let galleryBase64ImageString = [];
    if (this.editEventId != 'new') {
      this.galleryObj['EventId'] = this.editEventId;

      if (event.target.files && event.target.files[0]) {
        var filesLength = event.target.files.length;
        for (let i = 0; i < filesLength; i++) {

          var reader = new FileReader();
          reader.onload = (event: any) => {
            let toArray = event.target.result.split(","); // event.target.result.substr(23)    
            galleryBase64ImageString.push({ 'EventImageUrl': toArray[1] });
            this.galleryObj['Images'] = galleryBase64ImageString;
            this.gallaryEventsUrls.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]);
        }

      }
    } else {
      this.galleryErrorsObj['isErrorGalleryFlag'] = true;
      this.galleryErrorsObj['galleryErrorMessage'] = 'First create event then upload gallery photos'
      this.galleryError = this.galleryErrorsObj;
    }
  }

  /**
   * @author: Smita
   * @function use : onAddEventsPhotos function use 
   * @date:19-11-2019
   */

  onAddEventsPhotos() {
    this.ngxLoader.start();
    if (this.galleryObj && (Object.keys(this.galleryObj).length === 0)) {
      this.galleryErrorsObj['isErrorGalleryFlag'] = true;
      this.galleryErrorsObj['galleryErrorMessage'] = (this.editEventId != 'new') ? 'Please upload gallery photos.' : 'First create event then upload gallery photos.'
      this.galleryError = this.galleryErrorsObj;

    } else {
      this.galleryErrorsObj['isErrorGalleryFlag'] = false;
      this.galleryError = this.galleryErrorsObj;
      this.dashboardService.addEventGalleryPhotos(this.galleryObj).subscribe((data) => {
        if (data != '' && data != undefined) {
          this.ngxLoader.stop();
          if (data.IsSuccess == true) {
            this.getGalleryPhotos();
          }
        }
      }, error => {
        this.ngxLoader.stop();
        this.handleError(error);
      });
    }
  }

  /**
   * @author: smita
   * @function use: getGalleryPhotos functions use for get all uploaded events photos and show in gallery
   * @date: 20-11-2019
   */
  getGalleryPhotos() {

    if (this.editEventId != null && this.editEventId != undefined && this.editEventId != 'new') {
      this.dashboardService.getAllGalleryPhotos(this.editEventId).subscribe((data) => {
        if (data != undefined && data.length > 0) {
          this.galleryPhotos = data;
          this.galleryPhotosLength = data.length;
        }
      }, error => {
        this.ngxLoader.stop();
        this.galleryImgErrorMessage = error.error;
        // this.handleError(error);
      })
    }
  }

  removeGalleryPhoto(eventId, imageId, eventImageUrl) {
    let deleteData = [];
    if (eventId != '' && imageId != '' && eventImageUrl != '') {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You want to delete this Photo?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {
          this.deleteObj['EventId'] = eventId;
          this.deleteObj['ImageId'] = imageId;
          this.deleteObj['EventImageUrl'] = eventImageUrl;
          deleteData.push(this.deleteObj);
          this.dashboardService.deleteGalleryPhoto(deleteData).subscribe((data) => {
            this.ngxLoader.stop();
            console.log(data);
            this.getGalleryPhotos();
            Swal.fire(
              'Deleted!',
              'Your photo has been deleted.',
              'success'
            )
          }, error => {
            this.ngxLoader.stop();
            this.handleError(error);
          })


        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelled',
            'Your photo is safe :)',
            'error'
          )
        }
      })
    }
  }

  OnAutoClear() {
    this.eventsAddForm.controls['artist_name'].setValue('');
  }
  selectEvent(item) {
    this.eventsAddForm.controls['artist_name'].setValue(item.id);
    // do something with selected item
  }

  onChangeSearch(val: string) {
    console.log('val4444', val)
  }

  onFocused(e) {
    // do something when input is focused
  }

  handleError(error) {

    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', errorMessage);
      // window.alert(errorMessage);
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: errorMessage
      });
    } else {
      // server-side error
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      console.log('server-side error', this.ShowErrorMsg);
      let showErrorMessage = (this.ShowErrorMsg.message != '') ? this.ShowErrorMsg.message : this.ShowErrorMsg;
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      })
    }

    return throwError(errorMessage);
  }

}
