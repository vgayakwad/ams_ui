import { Component, OnInit,Renderer } from '@angular/core';
import { ActivatedRoute, Params, Router , NavigationEnd} from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-category-event-list',
  templateUrl: './category-event-list.component.html',
  styleUrls: ['./category-event-list.component.css'],
  providers: [DashboardService],
})
export class CategoryEventListComponent implements OnInit {
  eventCategoryId:any;
  eventsResponse:any;
  eventResponseLength:any;
  eventTypeName:any;
  errorMessage:any;
  constructor(public route: ActivatedRoute,
    private dashboardService: DashboardService,
    private ngxLoader: NgxUiLoaderService,
    private render: Renderer,
    private router: Router,) { 
    this.eventCategoryId = atob(this.route.snapshot.paramMap.get('paramA'));
  }

  ngOnInit() {
    this.categoryWiseEventList(this.eventCategoryId,'');
  }

 

  categoryWiseEventList(eventCatId,searchKey){

    if(this.eventCategoryId != undefined && this.eventCategoryId != null){
      this.eventCategoryId = eventCatId;
      this.ngxLoader.start();     
      this.dashboardService.eventListByCategoryId(this.eventCategoryId,searchKey).subscribe((data) => {
        if (Object.keys(data).length > 0) {
          this.ngxLoader.stop();
          this.eventTypeName = data[0].EventTypeName;          
          this.eventsResponse = data;
          this.eventResponseLength = Object.keys(data).length;        
        }
      }, error => {
        this.ngxLoader.stop();
        this.eventResponseLength =0;
        this.eventsResponse= [];
       // this.handleError(error);
      });
    }
    
  }

  screenRedirect(eventId){
    this.router.navigate(['/event-details/', btoa(eventId)])  
  }

  searchCategoryEvents(SearchKey){
    console.log('oooooo',SearchKey);   
    this.categoryWiseEventList(this.eventCategoryId,SearchKey)
  }
}
