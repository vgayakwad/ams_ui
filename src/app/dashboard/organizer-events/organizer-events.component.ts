import { Component, OnInit,Renderer,Inject } from '@angular/core';
import { ActivatedRoute, Params, Router , NavigationEnd} from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';
import { UtilService } from '../../services/util.service';
import { ProfileService } from '../../services/profile.service';
import { Observable, throwError, from } from 'rxjs';
import { map, catchError, retry, startWith } from 'rxjs/operators';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
declare var $: any;
@Component({
  selector: 'app-organizer-events',
  templateUrl: './organizer-events.component.html',
  styleUrls: ['./organizer-events.component.css'],
  providers: [DashboardService, UtilService ,ProfileService],
})
export class OrganizerEventsComponent implements OnInit {
  organizerId: any;
  eventsResponse:any;
  organizerPastEventDetails:any;
  organizerPastEventDetailsLength:any = 0;
  eventDataLength = 0;
  ShowErrorMsg:any;
  ShowErrorMsgOrganizerEvents:any;
  userName: any;
  OrganizationName:any;
  profileImage:any;
  aboutMe:any;
  Loginchecker:any = false;
  userType:any;
  loginId:any;
  searchText:any = ""
  eventsCategoryData:any;
  eventCategoryIdSelected : any = [];
  dateFilterRadioSelected:any = 'IsMonth';
  organizerRating:any = 0;
  OrganizerRatingErrorMessage: any ='';
  orgAverageRating:any;
  usersRating:any;
  ratingNumber:any =0;
  ipAddress:any;
  p1: number =1;
  p2:number =1;

  eventDateFilter : any = [{
    'name' :'Week',
    'value':'IsWeek'
  },
  {
    'name':'Today',
    'value':'IsToday'
  },
  {
    'name':'Tomorrow',
    'value':'IsTomorrow'
  },
  {
    'name':'Weekend',
    'value':'IsWeekend'
  },
  {
    'name':'Month',
    'value':'IsMonth'
  }
]

  constructor(public route: ActivatedRoute,
    private dashboardService: DashboardService,
    private utilService: UtilService,
    private profileService: ProfileService,
    private ngxLoader: NgxUiLoaderService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private render: Renderer,
    private router: Router,) {   
    this.organizerId = atob(this.route.snapshot.paramMap.get('paramA'));
    }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getEventsListByOrganizerId(this.searchText,[],this.dateFilterRadioSelected,this.organizerId);
    this.getPastEventListByOrganizerId(this.searchText,[],this.dateFilterRadioSelected,this.organizerId,this.ratingNumber);
    this.getEventsCategoryList();
    this.getSystemIp();
    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    if (loginLocalStorage != null && loginLocalStorage != undefined) {     
      this.loginId = loginLocalStorage.Id;
      this.Loginchecker = true;
      this.userType = loginLocalStorage.UserType;
     
    } else if (registerUserData != null && registerUserData != undefined) {     
      this.loginId = registerUserData.Id;
      this.Loginchecker = true;
      this.userType = registerUserData.UserType;
    }   

  }

  getEventsCategoryList(){
    this.ngxLoader.start();
    this.profileService.EventsCategoryList().subscribe((data) => {
      if (data.length > 0) {
        this.ngxLoader.stop();
        this.eventsCategoryData = data;
      }
    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
    });
  }

  getEventsListByOrganizerId(searchText, categoryIds, dateFilterRadioSelected,organizerId){
    this.organizerId = organizerId;
    if(this.organizerId!='' && this.organizerId!= undefined){
     let postObj = {
        "SearchTitle":searchText,
        "CategoryId": categoryIds,
        "DateFormat":dateFilterRadioSelected,
        "UserId": this.organizerId
      };
      this.dashboardService.getEventListByOrganizer(postObj).subscribe(data =>{
       
        if (Object.keys(data).length > 0) {         
          this.userName = (data.FirstName!='' && data.LastName!='')?data.FirstName+" "+data.LastName:'';
          this.OrganizationName = (data.Organization!='')?data.Organization:'';
          this.profileImage = (data.ProfileImageURL!=null)?data.ProfileImageURL:null;
          this.userType = (data.UserType!=null)?data.UserType:null;
          this.aboutMe =  data.Description;
          this.orgAverageRating = (data.AverageRating!=null)?data:0;
          this.usersRating = data.CustomerRating;

          console.log('5555',this.userName,'000',  this.OrganizationName,'iiii',this.profileImage );

          this.eventsResponse = (data.EventList!=undefined &&data.EventList.length>0)?data.EventList:[];
          this.eventDataLength = data.EventList.length;  
        }else{
          this.eventDataLength = 0;  
        }
      },error =>{
        this.ngxLoader.stop();
        this.handleError(error);
        this.ShowErrorMsgOrganizerEvents = error.error.message;

      })
    }
    
  }
  
  getPastEventListByOrganizerId(searchText,categoryIds,dateFilterRadioSelected,organizerId, ratingNumber){
    let postObj = {
      SearchTitle: searchText,
      CategoryId:categoryIds,
      //EventAttribute: "PASTEVENTS",
      IsPrivate: false,
      DateFormat: dateFilterRadioSelected,
      UserId: organizerId,
      //Type: "Event",
      Rating: ratingNumber,
      UserType:"Organizer"
    };

    this.dashboardService.getArtistOrganizerPastEvents(postObj).subscribe((data) => {
      if (Object.keys(data).length != 0) {
       // console.log('passs orgainzer eventssssssss', data);
        this.organizerPastEventDetails = data;
        this.organizerPastEventDetailsLength = data.length;
      }else{
        this.organizerPastEventDetailsLength = 0;
      }
    }, error => {
      this.organizerPastEventDetails = [];
      this.organizerPastEventDetailsLength= 0;
      this.ngxLoader.stop();
      this.ShowErrorMsgOrganizerEvents = error.error.message;
     // this.eventErrorMessage = error.error;
    })

  }

  screenRedirect(eventId,eventType){   

    if(eventId == 'new'){       
      this.router.navigate(['/events/', btoa('new'),btoa('false')]) 
    }else if(eventType == 'past'){
      this.router.navigate(['/event-details/', btoa(eventId),btoa('true')])            
    }else{
      this.router.navigate(['/event-details/', btoa(eventId),btoa('false')])   
    }
    
  }

  searchEventByRating(rating_no){
    console.log(rating_no);
    let ratingArray = rating_no.split(" ");
    this.ratingNumber = ratingArray[0];
    this.getPastEventListByOrganizerId(this.searchText,[],this.dateFilterRadioSelected,this.organizerId,this.ratingNumber);

  }

  onDateFilterItemChange(item){
    console.log(item);
    if(item.value!='' && item.value!=undefined){
      this.dateFilterRadioSelected = item.value;
      this.getEventsListByOrganizerId(this.searchText, [], this.dateFilterRadioSelected, this.organizerId);
      this.getPastEventListByOrganizerId(this.searchText,[],this.dateFilterRadioSelected,this.organizerId,this.ratingNumber);
    }
  }

  multiEventCategorySelect(e,categoryId){    
    if(e.target.checked && categoryId != null){
      this.eventCategoryIdSelected.push(categoryId);     
    }else{
      this.eventCategoryIdSelected = this.eventCategoryIdSelected.filter(id => id !== categoryId);  
    }
    this.getEventsListByOrganizerId(this.searchText, this.eventCategoryIdSelected, this.dateFilterRadioSelected, this.organizerId);
    this.getPastEventListByOrganizerId(this.searchText,this.eventCategoryIdSelected,this.dateFilterRadioSelected,this.organizerId,this.ratingNumber);
    
  }

  searchEvents(items){
    console.log('555', items);
    this.searchText = items;
    this.getEventsListByOrganizerId(this.searchText, this.eventCategoryIdSelected, this.dateFilterRadioSelected, this.organizerId);
    this.getPastEventListByOrganizerId(this.searchText,this.eventCategoryIdSelected,this.dateFilterRadioSelected,this.organizerId,this.ratingNumber);
    items
  }

  /**
   * @author : Smita
   * @function use : 'onOrganizerRate function use for click on star get value on star'
   * @date : 11-2-2020
   * @param event 
   */

  onOrganizerRate(event){
    this.organizerRating = event.newValue;
  }

   /**
   * @Author :smita
   * @function use : get system Ip
   * @date : 17-3-2020
   */
  getSystemIp(){      
    this.dashboardService.getSystemIp().subscribe((res) => {
      if(res!=null){
        this.ipAddress=res['ip'];
      }           
    }, error => {    
      this.handleError(error);
    })
   }

  /**
   * @author : Smita
   * @function use : "Client and Server side Error handling "
   * @date : 11-2-2020   * 
   */
    submitOrganizerRating(){

        if(this.organizerRating!=0 && this.organizerRating!=undefined && (this.ipAddress!='' && this.ipAddress!=undefined)){
          this.OrganizerRatingErrorMessage = '';
          let addRatingObject = {
            "EventOrUserId":this.organizerId,
             "Rating":this.organizerRating,
             "Type":'Organizer',
             "IPAddress": this.ipAddress
          } 

          this.dashboardService.addRating(addRatingObject).subscribe((data) => {
            console.log('data==',data);
            if(data!= null){
              this.organizerRating = 0;
              this.getEventsListByOrganizerId(this.searchText, this.eventCategoryIdSelected, this.dateFilterRadioSelected, this.organizerId);
              $("#myOrganizerModal").modal("hide");
              const Toast = Swal.mixin({
                toast: true,
                showConfirmButton: false,
                timer: 3000
              })
              Toast.fire({
                icon: 'success',
                title: 'Add Rating Successfully.'
              });
      
            }
           },error =>{
      
           })


        }else{
          this.OrganizerRatingErrorMessage = 'Please select at least 1 star';
        }
    }


  /**
   * @author : Smita
   * @function use : "Client and Server side Error handling "
   * @date : 11-2-2020
   * @param event 
   */
  
  handleError(error) {

    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', errorMessage);
      // window.alert(errorMessage);
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: errorMessage
      });
    } else {
      // server-side error
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      console.log('server-side error', this.ShowErrorMsg);
      let showErrorMessage = (this.ShowErrorMsg.message != '') ? this.ShowErrorMsg.message : this.ShowErrorMsg;
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      })
    }
    return throwError(errorMessage);
  }

 

}
