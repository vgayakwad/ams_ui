import { Component, OnInit, Inject, HostListener, ElementRef } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilService } from '../../services/util.service';
import Swal from 'sweetalert2';
import { Observable, throwError } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css'],
  providers: [DashboardService, UtilService]
})

export class EventDetailsComponent implements OnInit {

  eventDetails: any;
  eventDetailLength: any;
  ShowErrorMsg: any;
  eventId: any
  galleryPhotos: any;
  loginId: any;
  errorMessage: any;
  todayDate = moment(new Date()).format('YYYY-MM-DD');
  eventEndDate: any;
  galleryErrorMessage: any;
  eventDeleteObj: any = {};
  eventRating:any;
  eventRatingErrorMessage:any;
  ratingNumber: any = 0;
  isPast:any;
  ipAddress:any;
  // css_class: any;
  constructor(private route: ActivatedRoute, 
    private dashboardService: DashboardService, private router: Router,
    private utilService: UtilService,
    private ngxLoader: NgxUiLoaderService,
    private el: ElementRef,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) {
    // this.css_class = 'fix-to-top-gist';
    //alert('55')
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (document.body.scrollTop > 400 ||
      document.documentElement.scrollTop > 400) {

      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      
        document.getElementById('nav_is_private').setAttribute("style", "left:0;");
        document.getElementById('add_event').setAttribute("style", "left:0;");        
        document.getElementById('nav_sub_menu').classList.remove('fix-to-top-nav');
        document.getElementById('venue_details').classList.remove('fix-to-top-gist');
      } else {       
        document.getElementById('nav_sub_menu').classList.add('fix-to-top-nav');
        document.getElementById('venue_details').classList.add('fix-to-top-gist');
      }
    } else {
     // document.getElementById('nav_is_private').classList.add('toggDiv');
      document.getElementById('nav_sub_menu').classList.remove('fix-to-top-nav');
      document.getElementById('venue_details').classList.remove('fix-to-top-gist');
    }
  }

  ngOnInit() {

    const loginLocalStorage = this.storage.get('login');
    const registerUserData = this.storage.get("register");
    if (loginLocalStorage != null && loginLocalStorage != undefined) {
      this.loginId = loginLocalStorage.Id;
    } else if (registerUserData != null && registerUserData != undefined) {
      this.loginId = registerUserData.Id;
    }
    this.eventId = atob(this.route.snapshot.paramMap.get('paramA'));
    this.isPast = atob(this.route.snapshot.paramMap.get('paramB'));
    //alert(this.eventId);
    window.scrollTo(0, 0);
    this.getEventDetails();
    this.getGalleryPhotos();
    this.getSystemIp();
  }

  getEventDetails() {
    this.ngxLoader.start();

    this.dashboardService.getEventDetails(this.eventId,this.isPast).subscribe(res => {
      this.ngxLoader.stop();
      if (Object.keys(res).length > 0) {
        this.eventDetails = res;
        this.eventDetailLength = res.length;
        this.eventEndDate = moment(res.EventEnd).format('YYYY-MM-DD');
        console.log("event details", this.eventDetails)
      } else {
        alert("Event Details not found");
      }

    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
      // this.router.navigate(['/event-details',btoa(this.eventId)]);
    });
  }

  checkValue(isPrivate: any){
    console.log('checkeeeeeeeee',isPrivate);
    let EventPublicPrivateObj = {
      IsVisibility:true, 
      IsPrivate:isPrivate,
      EventId :this.eventId
    }
    this.dashboardService.addEvents(EventPublicPrivateObj).subscribe(result =>{
      if (result != null) {
        const Toast = Swal.mixin({
          toast: true,
          showConfirmButton: false,
          timer: 3000
        })
        Toast.fire({
          icon: 'success',
          title: 'Event Updated Successfully.'
        });
        this.router.navigate(['/event-details/', btoa(this.eventId)])             
      }
    },error =>{
      this.handleError(error);
    })
  }

  getGalleryPhotos() {

    if (this.eventId != null && this.eventId != undefined) {
      this.dashboardService.getAllGalleryPhotos(this.eventId).subscribe((data) => {
        if (data != undefined && data.length > 0) {
          this.galleryPhotos = data;
        }
      }, error => {
        this.ngxLoader.stop();
        this.galleryErrorMessage = error.error;
        //this.handleError(error);
      })
    }

  }

  /**
   * @author :smita
   * @param : navigateToSection use for highlight div when click on menu
   */
  navigateToSection(element): void {
    $('#' + element)[0].scrollIntoView();
  }

  /**
   * 
   * @param id 
   * @function use : showArtistDetails use for click on artist show artist info
   * @date : updated 13-12-2019 (smita)
   */
  showArtistDetails(id) {
    this.router.navigate(['/artist-events', btoa(id)]);
  }


  NavigateScreen(screen) {
    
    if (screen == 'DeleteEvent') {
      console.log('55555555555555555',this.eventDetails);
      let deleteData = [];
      if (this.eventId != '' && this.eventId!=undefined && this.eventDetails!= undefined 
       ) {
        Swal.fire({
          title: 'Are you sure?',
          text: 'You want to delete this event?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it'
        }).then((result) => {
          if (result.value) {
            this.ngxLoader.start();
            let artistArr = [];
            console.log('---------------',this.eventDetails.ArtistsList);
            this.eventDeleteObj['EventId'] = this.eventId;
            if(this.eventDetails.ArtistsList!=null && this.eventDetails.ArtistsList.length>0){
              for (var artist of this.eventDetails.ArtistsList){                 
                artistArr.push({'id':artist.id,'ScheduleId':artist.ScheduleId, 'AvailableFrom':artist.AvailableFrom,'AvailableTo':artist.AvailableTo })
              }
            }else{
              artistArr = [];
            }
            this.eventDeleteObj['ArtistsList'] = artistArr;
            console.log('---------------',this.eventDeleteObj);
            //this.eventDeleteObj['EventImageUrl'] = eventImageUrl;
           // deleteData.push(this.eventDeleteObj);
            this.dashboardService.deleteEvent(this.eventDeleteObj).subscribe((data) => {
              this.ngxLoader.stop();
              console.log('ressssssssssss',data);
              if(data.IsSuccess == true){
                Swal.fire(
                  'Deleted!',
                  'Your Event has been deleted.',
                  'success'
                )
                this.router.navigate(['/']);
              } 
            }, error => {
              this.ngxLoader.stop();
              this.handleError(error);
            })

          } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
              'Cancelled',
              'Your photo is safe :)',
              'error'
            )
          }
        })
      }

    }else if(screen == 'AddEvent'){
      this.router.navigate(['/events', btoa('new'),btoa(this.isPast)]);
    }else {
      this.router.navigate(['/events', btoa(this.eventId),btoa(this.isPast)]);
    }

  }

  /**
   * @Author : Smita
   * @function Use : onEventRate use for given/ assign rating to Event 
   * @param event 
   * @Date : 10-2-2020
   */
  onEventRate(event){
    console.log(event);
    this.ratingNumber = event.newValue;
    this.eventRating = event.newValue;
  }

  /**
   * @Author :smita
   * @function use : get system Ip
   * @date : 17-3-2020
   */
  getSystemIp(){      
    this.dashboardService.getSystemIp().subscribe((res) => {
      if(res!=null){
        this.ipAddress=res['ip'];
      }           
    }, error => {    
      this.handleError(error);
    })
   }

   /**
   * @Author : Smita
   * @function use : 'submitRating use for insert event rating.
   * @date : 12-2-2020   * 
   */

  submitRating(){
    
    if(this.eventRating!=0 && this.eventRating!= undefined && (this.ipAddress!='' && this.ipAddress!=undefined)){
      this.eventRatingErrorMessage = '';
alert(this.ipAddress);
      let addRatingObject = {
        "EventOrUserId":this.eventId,
         "Rating":this.eventRating,
         "Type":'Event',
         "IPAddress" : this.ipAddress
      }    
     this.dashboardService.addRating(addRatingObject).subscribe((data) => {
      console.log('data==',data);
      if(data!= null){
        this.ratingNumber = 0;
        $("#myModal").modal("hide");
        const Toast = Swal.mixin({
          toast: true,
          showConfirmButton: false,
          timer: 3000
        })
        Toast.fire({
          icon: 'success',
          title: 'Add Rating Successfully.'
        });
        this.getEventDetails();

      }
     },error =>{

     })


    }else{
    
      this.eventRatingErrorMessage = 'Please select at least 1 star.';
    }
  }

  handleError(error) {

    this.ShowErrorMsg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      this.errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', this.errorMessage)
    } else {
      // server-side error      
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      this.errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      console.log('server-side error', this.errorMessage);
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      });
    }
    return throwError(this.errorMessage);
  }

}
