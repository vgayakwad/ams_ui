import { Component, OnInit, Renderer, Inject } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ProfileService } from '../../services/profile.service';
import { DashboardService } from '../../services/dashboard.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-event-category',
  templateUrl: './event-category.component.html',
  styleUrls: ['./event-category.component.css'],
  providers: [ProfileService],
})
export class EventCategoryComponent implements OnInit {
  eventsCategoryData: any;
  loginId: any;
  userType: any;
  eventsCount: any;
  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    initialSlide: 0,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    'infinite': false,
    'mobileFirst': false,
    'focusOnSelect': false,
    'respondTo': 'window',
    rows: 1,
    'responsive': [{
      'breakpoint': 1024,
      'settings': {

        'slidesToShow': 1,
        'slidesToScroll': 1,
        'initialSlide': 1,
        'arrows': true,
        dots: true
      }
    }]
  };

  constructor(private profileService: ProfileService,
    private ngxLoader: NgxUiLoaderService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private dashboardService: DashboardService,
    private render: Renderer,
    private router: Router, ) { }

  ngOnInit() {
    this.getEventsCategoryList();


    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    if (loginLocalStorage != null && loginLocalStorage != undefined) {
      this.loginId = loginLocalStorage.Id;
      this.userType = loginLocalStorage.UserType;
    } else if (registerUserData != null && registerUserData != undefined) {
      this.loginId = registerUserData.Id;
      this.userType = registerUserData.UserType;
    }
  }

  /**
  * @author :smita
  * @function Use : getEventsCategoryList use for list of events category
  * @date : 10-1-2020
  *
  */
  getEventsCategoryList() {
    this.ngxLoader.start();
    this.profileService.EventsCategoryList().subscribe((data) => {
      if (data.length > 0) {
        this.ngxLoader.stop();
        data.filter((data) => {
          if (data.EventCount == null) {
            data.EventCount = 0;
          }
        });
        if (data != null && data != undefined) {
          this.dashboardService.eventCountByEventCategory().subscribe(res => {
            console.log('rrrrrr', res);
            this.eventsCount = res;
            data.filter((data) => {
              this.eventsCount.filter((newRes) => {
                if (data.EventCategoryName == newRes.EventCategoryName) {
                  data.EventCount = newRes.EventCount;
                }
              })
            })
            this.eventsCategoryData = data.sort((a, b) => ((a.EventCount > b.EventCount) ? -1 : 1
            ));
            console.log('77777', this.eventsCategoryData);
          }, error => {
            console.log(error);
            //this.handleError(error);
          })
        }

        // this.eventCountByEventCategory();
      }
    }, error => {
      this.ngxLoader.stop();
      // this.handleError(error);
    });
  }

  /**
   * @author : smita
   * @function use : eventCountByEventCategory use for show category wise events count
   * @date : 23-1-2020
   */
  // eventCountByEventCategory() {
  //   this.dashboardService.eventCountByEventCategory().subscribe(res => {
  //     console.log('rrrrrr', res);
  //     this.eventsCount = res;
  //     this.eventsCount.sort((a,b) => (a.EventCount > b.EventCount ? -1 : 1));
  //     console.log('elessssssssssss', this.eventsCategoryData,this.eventsCount);
  //     this.eventsCategoryData.filter((data) => {
  //       this.eventsCount.filter((newRes) => {  
  //         if (data.EventCategoryName == newRes.EventCategoryName) {
  //           //console.log('data.EventCategoryName==',data.EventCategoryName, 'newRes.EventCategoryName===',newRes.EventCategoryName)
  //           data.EventCount = newRes.EventCount;
  //         }
  //       })
  //     })         
  //     // this.eventsCategoryData.sort((a,b) => ((a.EventCount > b.EventCount) ? -1 : 1
  //     // ));
  //    // console.log(' this.eventsCategoryData==', this.eventsCategoryData);    
  //     console.log('77777', this.eventsCategoryData);
  //   }, error => {

  //   })
  //   // GetEventCountByCategory
  // }

  clickOnEventCategory(eventCategoryId) {
    console.log('eeee', eventCategoryId);
    this.router.navigate(['/category-event-list/', btoa(eventCategoryId)])
  }
  afterChange(e) {
    console.log('afterChange');
  }

  beforeChange(e) {
    console.log('beforeChange');
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }
}
