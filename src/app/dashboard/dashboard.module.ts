import { NgModule,LOCALE_ID  } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { BannerComponent } from './banner/banner.component';

import { EventsComponent } from './events/events.component';

import { BrowserModule } from '@angular/platform-browser';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {NgxPaginationModule} from 'ngx-pagination';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { ArtistEventScheduleComponent } from './artist-event-schedule/artist-event-schedule.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { RatingModule } from 'ng-starrating';
import {ProgressBarModule} from "angular-progress-bar";
import { ShareButtonsModule } from '@ngx-share/buttons';

import { DashboardService } from '../services/dashboard.service';
import { ValidationService } from '../services/validation.service';
import { ProfileService} from '../services/profile.service';
import { OrganizerEventsComponent } from './organizer-events/organizer-events.component';
import { EventCategoryComponent } from './event-category/event-category.component';
import { SkillsEventsComponent } from './skills-events/skills-events.component';
import { CategoryEventListComponent } from './category-event-list/category-event-list.component';
// import { MatAutocompleteModule, MatFormFieldModule,MatInputModule} from '@angular/material';
// import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [EventListComponent, EventDetailsComponent, SearchBarComponent, BannerComponent,  EventsComponent, ArtistEventScheduleComponent,   OrganizerEventsComponent,EventCategoryComponent, SkillsEventsComponent, CategoryEventListComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    SlickCarouselModule,
    RatingModule,   
    ProgressBarModule, 
    ShareButtonsModule,
    // MatAutocompleteModule, 
    // MatFormFieldModule,
    // MatInputModule,
    // MatSelectModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgxPaginationModule,
    NgxUiLoaderModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDrTdFn5n9tvR6qM6YFOjH7wswRvcz_khQ',
      libraries: ['places']
    }),
    MatGoogleMapsAutocompleteModule,
    FullCalendarModule
  ],
  //Export component to access in root
  exports: [
    EventListComponent,
    SearchBarComponent,
    EventDetailsComponent,
    BannerComponent,
    OrganizerEventsComponent,
  ],
  providers: [
    DashboardService,
    ValidationService,
    ProfileService,        
  ],
})
export class DashboardModule { }
