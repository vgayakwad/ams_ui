import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {EventListComponent} from './dashboard/event-list/event-list.component';
import {EventDetailsComponent} from './dashboard/event-details/event-details.component';
import {RegistrationComponent } from './auth/registration/registration.component';
import { LoginComponent } from './auth/login/login.component';
import { ProfileComponent } from './auth/profile/profile.component';
import { EventsComponent } from './dashboard/events/events.component';
import { ViewProfileComponent } from './auth/view-profile/view-profile.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import {AuthGuard} from './auth.guard';
import { ArtistEventScheduleComponent } from './dashboard/artist-event-schedule/artist-event-schedule.component';
import { CreateArtistScheduleComponent } from './artistsinfo/create-artist-schedule/create-artist-schedule.component';
import { ArtistComponent } from './artistsinfo/artist/artist.component';

import { OrganizerEventsComponent } from './dashboard/organizer-events/organizer-events.component';
import {OrganizerComponent} from './organization/organizer/organizer.component';
import { NotificationComponent } from './organization/notification/notification.component';
import { SkillsEventsComponent } from './dashboard/skills-events/skills-events.component';
import { CategoryEventListComponent } from './dashboard/category-event-list/category-event-list.component';
import { from } from 'rxjs';

const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: 'Login', component: LoginComponent },
  { path: 'Registration/:paramA' , component: RegistrationComponent },
  { path: 'profile',canActivate:[AuthGuard],component:ProfileComponent}, 
  { path: 'events/:paramA/:paramB', canActivate:[AuthGuard],component: EventsComponent},
  { path: 'view-profile', canActivate:[AuthGuard],component: ViewProfileComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'event-details/:paramA/:paramB', component: EventDetailsComponent },
  { path: 'artist-events/:paramA', component: ArtistEventScheduleComponent },
  { path: 'create-artist-schedule/:paramA', canActivate:[AuthGuard], component: CreateArtistScheduleComponent },
  { path: 'artist', component: ArtistComponent },
  { path: 'category-event-list/:paramA' , component:CategoryEventListComponent},
  { path: 'organizer-events/:paramA', component:OrganizerEventsComponent},
  { path: 'organizer', canActivate:[AuthGuard], component:OrganizerComponent}, 
  { path: 'notification', canActivate:[AuthGuard], component:NotificationComponent},
  { path: 'skills-events/:paramA', component:SkillsEventsComponent},
  { path: 'event-list', component: EventListComponent },
  { path: '**', component: PageNotFoundComponent },    
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
