import { Component,Inject } from '@angular/core';
import { Router,NavigationStart } from '@angular/router';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
// import { NGXLogger } from 'ngx-logger';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AMS';
  showHead: boolean = true;
  currentUrl:any;
  constructor(public router:Router, 
    @Inject(LOCAL_STORAGE) private storage: StorageService){
    // this.logger.debug("Debug message");
    // this.logger.info("Info message");
    // this.logger.log("Default log message");
    // this.logger.warn("Warning message");
    // this.logger.error("Error message");
    // on route change to '/login', set the variable showHead to false
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.currentUrl = event.url; 
        this.storage.set('currentUrl', this.currentUrl);
        console.log(this.currentUrl,"Current URL");  
        if (event['url'] == '/Login') {
          this.showHead = false;
        } else {
          // console.log("NU")
          this.showHead = true;
        }
      }
    });
  }
  
}
