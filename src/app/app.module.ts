import { BrowserModule } from '@angular/platform-browser';
import { OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule,LOCALE_ID  } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
//Module Reference 
// import { AuthModule } from './auth/auth.module';
import { AuthServiceService } from './services/auth-service.service';
import { DashboardService } from './services/dashboard.service';
import { ValidationService } from './services/validation.service';
import { ProfileService} from './services/profile.service';

import { DashboardModule } from './dashboard/dashboard.module';
import { StaticpagesModule } from './staticpages/staticpages.module';
//import { MatAutocompleteModule, MatFormFieldModule,MatInputModule} from '@angular/material';
//import {MatSelectModule} from '@angular/material/select';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { HashLocationStrategy,Location, LocationStrategy } from '@angular/common';
import { from } from 'rxjs';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { ApiInterceptor } from './ApiInterceptor';
//import { IpAddressInterceptor } from './ApiInterceptor';
import { StorageServiceModule } from 'ngx-webstorage-service';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {NgxPaginationModule} from 'ngx-pagination';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
import { FullCalendarModule } from '@fullcalendar/angular';
import { InternationalPhoneNumber2Module } from 'ngx-international-phone-number2';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { RatingModule } from 'ng-starrating';
import {ProgressBarModule} from "angular-progress-bar";

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { ShareButtonsModule } from '@ngx-share/buttons';

import { ProfileComponent } from './auth/profile/profile.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { ViewProfileComponent } from './auth/view-profile/view-profile.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { OrganizerComponent } from './organization/organizer/organizer.component';
import { CreateArtistScheduleComponent } from './artistsinfo/create-artist-schedule/create-artist-schedule.component';
import { ArtistComponent } from './artistsinfo/artist/artist.component';
import { NotificationComponent } from './organization/notification/notification.component';
//import { EventCategoryComponent } from './dashboard/event-category/event-category.component';
//658198664083-dfbu6rsdi68i8gksuud8kok7phejmbns.apps.googleusercontent.com
//aVHaQUDq9pnm7f3dd4wf1r7q
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("658198664083-dfbu6rsdi68i8gksuud8kok7phejmbns.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("1493045580861119")
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    ProfileComponent,
    LoginComponent,
    RegistrationComponent,
    ViewProfileComponent,
    ForgotPasswordComponent,
    OrganizerComponent,
    CreateArtistScheduleComponent,
    ArtistComponent,
    NotificationComponent
    //EventCategoryComponent
   
  ],
  imports: [
    // AuthModule,
    BrowserModule,      
    DashboardModule,
    StaticpagesModule,
    AppRoutingModule,
    SlickCarouselModule,
    RatingModule, 
    ProgressBarModule,   
    SocialLoginModule,
    ShareButtonsModule,
    // MatAutocompleteModule, 
    // MatFormFieldModule,
    // MatInputModule,
    // MatSelectModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    StorageServiceModule ,
    AutocompleteLibModule,
    NgxUiLoaderModule,
    InternationalPhoneNumberModule,
    FullCalendarModule,
    //RouterModule.forRoot(routes),
    LoggerModule.forRoot({
      // serverLoggingUrl: '/api/logs',
      level: NgxLoggerLevel.TRACE,
      serverLogLevel: NgxLoggerLevel.ERROR,
      disableConsoleLogging: false
    }),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDrTdFn5n9tvR6qM6YFOjH7wswRvcz_khQ',
      libraries: ['places']
    }),
  MatGoogleMapsAutocompleteModule.forRoot(),
  InternationalPhoneNumber2Module

  ],
  providers: [AuthServiceService,   
    DashboardService,
    ValidationService,
    ProfileService,
    Location,
    {provide: AuthServiceConfig, useFactory: provideConfig },
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },   
    //{ provide: HTTP_INTERCEPTORS, useClass: IpAddressInterceptor, multi: true}, 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
