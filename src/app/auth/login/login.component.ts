import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthServiceService } from '../../services/auth-service.service';
import { Observable, throwError } from 'rxjs';
import { UtilService } from '../../services/util.service';
import Swal from 'sweetalert2';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider, SocialUser } from "angularx-social-login";
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  response: any;
  errorMsg: String;
  ShowErrorMsg: any;
  userType: String;
  socialLoginDetails: any;
  private socialSiteUserObj: SocialUser;
  private socialLoggedIn: boolean;

  constructor(private formBuilder: FormBuilder, private authenticationService: AuthServiceService, private router: Router, private utilService: UtilService,
    private ngxLoader: NgxUiLoaderService,
    private authService: AuthService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, ) { }

  ngOnInit() {

    const registerUserData = this.storage.get("register");
    const loginLocalStorage = this.storage.get('login');
    if (registerUserData != null || loginLocalStorage != null) {
      this.router.navigate(['/'])
    }
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      Password: ['', [Validators.required, Validators.minLength(6)]]
    });


    this.authService.authState.subscribe((user) => {

      if (user != null) {
        this.socialSiteUserObj = user;
        this.onLoginSubmit('', user);
      }
      this.socialLoggedIn = (user != null);
    });


  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authService.signOut();
  }


  /**
 * @author : Snehal
 * @function use : "get form control name to apply validation in front end"
 * @date : 10-12-2019
 */
  get f() { return this.loginForm.controls; }
  //end

  /**
 * @author : Snehal
 * @param loginCredential
 * @function use : "Called  api for post login credentials "
 * @date : 10-10-2019
 */
  onLoginSubmit(loginCredential, socialLoginData) {
    this.ngxLoader.start();

    if (loginCredential != '') {
      this.submitted = true;
    }

    if ((loginCredential != '' && (loginCredential.email && loginCredential.Password) != ''
      && (loginCredential.email && loginCredential.Password) != undefined)) {

      this.authenticationService.Login(loginCredential).subscribe(userLogindata => {
        this.ngxLoader.stop();
        if (userLogindata.AccessToken) {
          const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 3000
          })
          Toast.fire({
            icon: 'success',
            title: 'Signed in successfully.'
          });
          console.log('***********', userLogindata);
          if (userLogindata.UserType == "Organizer") {
            this.router.navigate(['/organizer-events/', btoa(userLogindata.Id)]);
          } else if (userLogindata.UserType == "Artist") {
            this.router.navigate(['/artist-events', btoa(userLogindata.Id)]);
          } else {
            this.router.navigate(['/']);
          }
        }
      }, error => {
        this.ngxLoader.stop();
        this.handleError(error);
        this.loginForm.get('Password').setValue('');
      })
    } else {

      if (socialLoginData != '' && socialLoginData != undefined) {
        
        this.socialLoginDetails = socialLoginData;
        if (socialLoginData.provider === "GOOGLE") {
          this.ngxLoader.stop();         
          this.authenticationService.CheckUserExist(socialLoginData.email, socialLoginData.provider).subscribe((result) => {
            console.log('social login result=====check===', result);
            if (Object.keys(result).length > 0 && result.IsValid == true) {
              this.storage.set('login', result);
              const Toast = Swal.mixin({
                toast: true,
                showConfirmButton: false,
                timer: 3000
              })
              Toast.fire({
                icon: 'success',
                title: 'Signed in successfully.'
              });              
              if (result.UserType == "Organizer") {
                this.router.navigate(['/organizer-events/', btoa(result.Id)]);
              } else if (result.UserType == "Artist") {
                this.router.navigate(['/artist-events', btoa(result.Id)]);
              } else {
                this.router.navigate(['/']);
              }

            } else {
              $("#SocialLoginModal").modal("show");
               this.socialLogin(); 
            }
          }, error => {
            console.log('social login error', error);
          })
        } else if (socialLoginData.provider === "FACEBOOK") {
          this.ngxLoader.stop();

        }
      } else {
        this.ngxLoader.stop();
        this.router.navigate(['/Login']);
      }
    }
  }  //end

  socialLoginUser(userType) {
    this.userType = userType;
    $("#SocialLoginModal").modal("hide");
    this.socialLogin();
    // let promise = new Promise((resolve, reject) => {      
    //   resolve();
    // });
    // return promise;

  }


  async socialLogin() {

    if (this.socialLoginDetails != '' && this.userType != '' && this.userType != undefined) {
      let socialUserRegister = {
        FirstName: this.socialLoginDetails.firstName,
        LastName: this.socialLoginDetails.lastName,
        MiddleName: '',
        Email: this.socialLoginDetails.email,
        Password: '',
        UserType: this.userType,
        Provider: this.socialLoginDetails.provider,
      }   

      this.authenticationService.Register(socialUserRegister).subscribe((data) => {
        this.storage.set('login', data);
        console.log('login regisert', data);
      })
      console.log('222222222===', socialUserRegister);
    }


  }

  /**
   * @author : Snehal
   * @param error
   * @function use : "Client and Server side Error handling"
   * @date : 13-11-2019
   */
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', errorMessage)
    } else {
      // server-side error
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      console.log('server-side error', errorMessage);
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.ShowErrorMsg
      });
    }
    return throwError(errorMessage);
  }
  //end


  /**
  * @author : Snehal
  * @function use : "send userType as artist and navigate to registration page."
  * @date : 14-10-2019
  */
  artistCreateProfile() {
    this.userType = "Artist";
    this.router.navigate(['/Registration', this.userType]);
  }
  //end

  /**
  * @author : Snehal
  * @function use : "send userType as organizer and navigate to registration page."
  * @date : 14-10-2019
  */
  organizerCreateProfile() {
    this.userType = "Organizer";
    this.router.navigate(['/Registration', this.userType]);
  }
  //end

  /**
  * @author : Snehal
  * @function use : "Navigate to forgot password page."
  * @date : 15-10-2019
  */
  forgotpassword() {
    this.router.navigate(['/forgot-password']);
  }
  //end

  /**
 * @author : Snehal
 * @param event
 * @function use : "Restrick user to enter space as password."
 * @date : 16-10-2019
 */
  _keyPress(event: any) {
    var k = event ? event.which : event.keyCode;
    if (k == 32) return false;
  }
  //end
}
