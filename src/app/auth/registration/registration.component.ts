import { Component, OnInit,Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthServiceService} from '../../services/auth-service.service';
import Swal from 'sweetalert2';
import {UtilService} from '../../services/util.service';
import { Observable, throwError } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

 registrationForm: FormGroup;
 userType: String;
 mailExistsError: String;
 ErrorMsg: Boolean=false;
 ShowErrorMsg: any;
 submitted:boolean = false;
 
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthServiceService,
    private router: Router,private utilService:UtilService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private ngxLoader: NgxUiLoaderService) { }

  ngOnInit() {
    this.ErrorMsg = false;
    this.userType=this.route.snapshot.paramMap.get('paramA');

    this.registrationForm= this.formBuilder.group({
      FirstName:['',[Validators.required]],
      LastName:['',[Validators.required]],
      MiddleName:[''],
      Email:['',[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      Password:['',[Validators.required,Validators.minLength(6)]],
      ConfirmPassword:['',[Validators.required,Validators.minLength(6)]]
    }, {
      validator: this.utilService.MustMatch('Password', 'ConfirmPassword')
  });
  }

  
  /**
   * @author : Snehal
   * @function use : "get form control name to apply validation in front end"
   * @date : 10-12-2019
   */
  get f() { return this.registrationForm.controls; }
  //end
 
  /**
   * @author : Snehal
   * @param form
   * @function use : "Called register api on signup button"
   * @date : 14-11-2019
   */
  onRegisterSubmit(form){
    this.ngxLoader.start();
    this.submitted =true;
    form.value.UserType= this.userType;
    console.log(form.value);
    if(form.valid){
      this.authenticationService.Register(form.value).subscribe(userRegisterdata=> {
        this.ngxLoader.stop();
        this.storage.set('register', userRegisterdata);
        if(userRegisterdata.AccessToken){
          const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 3000
          })
          Toast.fire({
            icon: 'success',
            title: 'You have successfully registered!'
          });
          //,btoa('0')
          this.router.navigate(['/profile']);
        }
      }, error => {
        this.ngxLoader.stop();
        this.handleError(error);
        this.router.navigate(['/Registration',this.userType]);
      });
    }
    else{
      this.ngxLoader.stop();
      console.log("invalid",form.valid);
      this.router.navigate(['/Registration',this.userType]);
    }
    
  }
  //end


     /**
   * @author : Snehal
   * @param error
   * @function use : "Client and Server side Error handling "
   * @date : 13-11-2019
   */
  handleError(error) {
    let errorMessage = '';
    this.ShowErrorMsg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', errorMessage)
    } else {
      // server-side error
      this.ShowErrorMsg= this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      console.log('server-side error', errorMessage);
      this.router.navigate(['/Registration',this.userType]);
     const Toast = Swal.mixin({
      toast: true,
      showConfirmButton: false,
      timer: 3000
    })
    Toast.fire({
      icon: 'error',
      title: this.ShowErrorMsg
    });
    }
    return throwError(errorMessage);
  }
     /**
   * @author : Snehal
   * @param event
   * @function use : "Restrick user to enter space as password. "
   * @date : 14-11-2019
   */
  _keyPress(event: any) {
    var k = event ? event.which : event.keyCode;
    if (k == 32) return false;
  }
  //end
}
