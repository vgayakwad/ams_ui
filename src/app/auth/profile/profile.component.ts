import { Component, OnInit, Inject, HostListener, ApplicationRef, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { Router, ActivatedRoute, } from '@angular/router';
import { ProfileService } from '../../services/profile.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { UtilService } from '../../services/util.service';
import Swal from 'sweetalert2';
import { Location, Appearance } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ProfileService, UtilService]

})

export class ProfileComponent implements OnInit {

  public maxDate = new Date();
  // For international phone number.
  model: any = {

  };
  createProfileForm: FormGroup;
  selectPlaceholder = '--select--';

  //upload image
  showProfile: boolean = false;
  showCity: boolean = false;
  showArtistCategory: boolean = false;
  fileData: File = null;
  UserData: any;
  GetUserData = {};
  previewUrl: string = '';
  previewdata: any;
  previewImage: any;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  SelCountryId: any;
  SelStateId: any;
  getCountry = 'US';
  getState: any;
  city_id: any;
  // getArtist=2;
  UserDate: any;
  public username: string;
  artistList: any;
  countryList: any;
  cityList: any;
  stateList: any;
  Organization: any;
  Address: any;
  UpdateProfile: boolean = false;

  showArtistCategorydropDown: boolean = false;
  localStorageLogin: any;
  localStorageRegister: any;
  myUserType: any;
  dataRefresher: any;

  ShowErrorMsg: any;
  errorMsg: any;
  errorMessage: any;
  isError: boolean = false;
  detail: boolean;
  userStorageData: any;

  max: any = moment().format('MM/DD/YYYY');
  event: any;
  birth: any;
  b_date: any;
  showMsg: any;
  BirthDateError: boolean = false;
  previewEditProfileURL: any;

  UserDetails: any;
  showLocation: Boolean = false;

  getCity: any;
  getTimeZoneOffset: any;
  timeZoneId: any;
  getPostalCode: any;
  markers: [{}];
  allTimeZone: any;
  public latitude: number;
  public longitude: number;
  public selectedAddress: PlaceResult;
  public zoom: number;
  updatedAddress: any;
  public appearance = Appearance;
  IsProfileImageUpdated: Boolean;
  defaultFlagCountry = 'us';
  phoneValid: Boolean;
  profileId:any;
  submitted:boolean = false;
  phoneNumber:any;
  BindArtistCategory=[];
  //skillset
  showTextboxSkill: boolean=false;
  BindSkillList=[];
  skill=[];
  // selectedSkillList:any;
  // selectedSkillsId=[];
  // selectedSkillNames=[];
  MoreSkills:any;
  duplicateErrorMsg:boolean=false;
  checkSkillError:number=0;
  constructor(private router: Router, private formBuilder: FormBuilder, private profileService: ProfileService,
    @Inject(LOCAL_STORAGE)
    private storage: StorageService,
    private utilService: UtilService,
    private ngxLoader: NgxUiLoaderService,
    location: LocationStrategy,
    private route: ActivatedRoute,
    private applicationRef: ApplicationRef,
    private zone: NgZone) {

    this.profileId = atob(this.route.snapshot.paramMap.get('paramA'));
    
    router.events.subscribe(() => {
      zone.run(() =>
        setTimeout(() => {
          this.applicationRef.tick();
        }, 0)
      );
    });
  }

  ngOnInit() {
    this.ngxLoader.start();
    this.latitude = 47.608013;
    this.longitude = -122.335167;

    this.onCountryChange(this.getCountry);
    this.getCountryList();
    this.getArtistCategoryList();
    this.getUserDetail();
    this.getSkillDropdownList();

    this.localStorageLogin = this.storage.get("login");
    this.localStorageRegister = this.storage.get("register");
    if (this.localStorageLogin != null) {
      this.userStorageData = this.localStorageLogin;
      this.myUserType = this.localStorageLogin.UserType;
      if (this.localStorageLogin.UserType == "Artist") {
        this.showArtistCategorydropDown = true;
      }
    }
     if (this.localStorageRegister != null) {
      this.userStorageData = this.localStorageRegister;
      this.myUserType = this.localStorageRegister.UserType;
      if (this.localStorageRegister.UserType == "Artist") {
        this.showArtistCategorydropDown = true;
      }
    }

    this.createProfileForm = this.formBuilder.group({
      ContactPerson: [''],
      PhoneNumber: ['',[Validators.required]],
      ContactEmail: ['', [Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      BirthDate: [null, [Validators.required]],
      CountryCode: [null, [Validators.required]],
      StateCode: ['', [Validators.required]],
      ArtistCategoryId: [null, [Validators.required]],
      CityId: ['', [Validators.required]],
      Gender: ['', [Validators.required]],
      FirstName: ['', [Validators.required]],
      LastName: ['', [Validators.required]],
      MiddleName: [''],
      Description: [''],
      Organization: [''],
      Address: [''],
      ProfileImage: [''],
      ArtistSkills:['', [Validators.required]],
      SkillsName:['']
    });
  }

  /**
   * @author : Snehal
   * @function use : "get form control name to apply validation in front end"
   * @date : 10-12-2019
   */
  get f() { return this.createProfileForm.controls; }
 //end
  
  /**
   * @author : Snehal
   * @function use : "set the flag for validation on change of artist catergory."
   * @date : 21-10-2019
   */
  onArtistCategoryChange() {
    this.showArtistCategory = false;
  }
  //end


  /**
   * @author : Snehal
   * @function use : "get artist category list for dropdown"
   * @date : 21-10-2019
   */
  getArtistCategoryList() {
    this.ngxLoader.start();
    this.profileService.artistCategoryList().subscribe((data: any) => {
      this.ngxLoader.stop();
      if (data.length > 0) {
        this.artistList = data;
      } else {
        alert("Artist List is not found");
      }

    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
    });
  }
  //end

    /**
   * @author : Snehal
   * @function use : "get country list for dropdown"
   * @date : 21-10-2019
   */
  getCountryList() {
    this.profileService.CountryList().subscribe((data) => {
      if (data.length > 0) {
        this.countryList = data;
      } else {
        alert("Country list is not found");
      }
    }, error => {
      this.handleError(error);
    });
  }
  //end

      /**
   * @author : Snehal
   * @param SelCountryCode
   * @function use : "on country change get state list for dropdown."
   * @date : 22-10-2019
   */
  onCountryChange(SelCountryCode) {
    this.getCountry = SelCountryCode;
    this.profileService.StateList(this.getCountry).subscribe((data: any) => {
      if (data.length > 0) {
        this.stateList = data;
      } else {
        alert("State list is not found");
      }
    }, error => {
      this.handleError(error);
    });
  }
  //end

  
    /**
   * @author : Snehal
   * @param SelStateCode
   * @function use : "on state change get city list for dropdown"
   * @date : 22-10-2019
   */
  onStateChange(SelStateCode) {
    this.city_id = "";
    this.getState = SelStateCode;
    this.profileService.CityList(this.getState).subscribe((data: any) => {
      if (data.length > 0) {
        data.filter(element => {
          if (element.CityName == this.getCity) {
            this.city_id = element.Id;
          }
        });
        this.cityList = data;
        this.showCity = false;
      } else {
        alert("Citylist is not found");
      }
    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
    });
  }
  //end


   /**
   * @author : Snehal
   * @function use : "set  flag for hide the  border-bottom line of address."
   * @date : 23-10-2019
   */
  showAddress() {
    this.showLocation = false;
  }
  //end


  /**
   * @author : Snehal
   * @function use : "check the condition- As artist age should be greater than 5 year."
   * @date : 23-10-2019
   */
  myBirthday() {
    this.event = this.createProfileForm.value.BirthDate;
    if (this.event != null) {
      this.b_date = moment(this.event).format('YYYY');
      this.max = moment().format('YYYY');
      this.birth = this.max - this.b_date;
      if (this.birth < 5) {
        this.BirthDateError = true;
        this.showMsg = "Not Eligible as your age is less than 5";
      } else {
        this.BirthDateError = false;
        this.showMsg = "";
      }
    }
  }
  //end

  /**
   * @author : Snehal
   * @param result
   * @function use : "get the state and city on selection of address from google-map "
   * @date : 23-10-2019
   */
  onAutocompleteSelected(result: PlaceResult) {
   if (result.address_components != null) {
      this.createProfileForm.controls['Address'].setValue(result.formatted_address);
      for (var i = 0; i < result.address_components.length; i++) {
        var addr = result.address_components[i];
        switch (addr.types[0]) {
          case 'locality': this.getCity = addr.long_name;
            break;
          case 'administrative_area_level_1': this.getState = addr.short_name;
            break;
          case 'country': this.getCountry = addr.short_name;
            break;
          case 'postal_code': this.getPostalCode = addr.long_name;
            break;
          default:
        }

        if (this.getCountry != '' && this.getCountry != undefined) {
          this.onCountryChange(this.getCountry);
        }
        if (this.getState != '' && this.getState != undefined) {
          this.onStateChange(this.getState);
        }
      }
    } else {
      alert('Address not found')
    }
  }
  onLocationSelected(location: Location) {
    this.latitude = location.latitude;
    this.longitude = location.longitude;
  }
//end



  /**
   * @author : Snehal
   * @function use : "get skill dropdown list and bind that list to skill field in html "
   * @date : 06-01-2020.
   */
getSkillDropdownList(){
  this.profileService.getSkillDropdownList().subscribe(res=>{
    console.log("skill dropdown",res);
    this.skill =res;
  })
}
//end

  /**
   * @author : Snehal
   * @function use : "get user profile details for edit profile."
   * @date : 7-11-2019
   */
  getUserDetail() {
    this.ngxLoader.start();
    this.profileService.getUserDetails().subscribe((data) => {
      if (data.CityId == 0 && data.ArtistCategoryId == 0) {
        data.CityId = "";
        data.ArtistCategoryId = "";
      }

      this.ngxLoader.stop();
      if (data != null) {
        this.UserData = data;
        console.log(".....",this.UserData);
        if(this.UserData.ArtistCategoryList != null){
          this.UserData.ArtistCategoryList.filter(artistCategoryId=>{
            this.BindArtistCategory.push(artistCategoryId.Id);
          })
        } 
        if(this.UserData.ArtistSkillsList != null){
          this.UserData.ArtistSkillsList.filter(skillId=>{
            this.BindSkillList.push(skillId.Id);
          })
        }
        if(this.UserData.PhoneNumber != null && this.UserData.PhoneNumber != undefined){
          this.phoneNumber = this.UserData.PhoneNumber;
        }
        if (this.UserData.BirthDate != null) {
          this.UserDate = new Date(this.UserData.BirthDate);
        }
        if (this.UserData.StateCode != null) {
          this.onStateChange(this.UserData.StateCode);
        }

        this.city_id = this.UserData.CityId;
        if (this.UserData.Address != "" && this.UserData.Address != undefined) {
          this.showLocation = true;
        }
        this.previewEditProfileURL = this.UserData.ProfileImageURL;
        if (this.previewEditProfileURL != null) {
          this.showProfile = false;
        }
      } else {
        this.ngxLoader.stop();
        alert("UserDetails is not found");
      }
    }, error => {
      this.ngxLoader.stop();
      this.handleError(error);
    });
  }
  //end

   /**
   * @author : Snehal
   * @function use : "To show textarea when click on Add skills button to add more skills for user."
   * @date : 09-01-2020
   */
  addExtraSkills(){
    this.showTextboxSkill = true;
  }
  //end

  /**
   * @author : Snehal
   * @function use : "To Remove textarea box of more skills on click of close button"
   * @date : 09-01-2020
   */
  RemoveExtraSkills(){
    this.showTextboxSkill = false;
    this.createProfileForm.get('SkillsName').setValue('');
    this.duplicateErrorMsg =false;
  }
  //end


   /**
   * @author : Snehal
   * @param SelectedSkillList
   * @function use : "Apply validation for texarea box when user not selected any dropdown skills "
   * @date : 09-01-2020
   */
  onSelectionChangedSkill(SelectedSkillList){
    console.log("select",SelectedSkillList)
    if(SelectedSkillList.length != 0 ){
      this.createProfileForm.controls['SkillsName'].clearValidators();
      this.createProfileForm.controls['SkillsName'].setErrors(null);
    }
    else{
      this.createProfileForm.controls['SkillsName'].setValidators([Validators.required]);
      this.createProfileForm.controls["SkillsName"].updateValueAndValidity();
    }
  }
  //end


  /**
   * @author : Snehal
   * @param text
   * @function use : "Apply validation for dropdown skills when user not added more skills"
   * @date : 09-01-2020
   */
  onTextareaChangeSkill(text){
    console.log("text",text);
    if(text != ""){
      this.createProfileForm.controls['ArtistSkills'].clearValidators();
      this.createProfileForm.controls['ArtistSkills'].setErrors(null);
    }
    else{
      this.createProfileForm.controls['ArtistSkills'].setValidators([Validators.required]);
      this.createProfileForm.controls["ArtistSkills"].updateValueAndValidity();
      this.duplicateErrorMsg =false;
    }  
  }
  //end

  /**
   * @author : Snehal
   *  @param form
   * @function use : "post user profile details on submit button."
   * @date : 8-11-2019
   */
  onCreateProfileSubmit(form) {
    this.ngxLoader.start();
    this.submitted =true;
    this.MoreSkills = (this.createProfileForm.get('SkillsName').value).split(',');
    this.checkSkillError = 0;
    if(this.MoreSkills != "" && this.MoreSkills != undefined)
    this.MoreSkills.filter(skillName=>{
      this.skill.filter(skills=>{
        if(skills.SkillsName.toLowerCase() == skillName){
          this.checkSkillError = 1;
        }
      })    
    })
    this.duplicateErrorMsg = (this.checkSkillError == 1)?true:false;
    form.value.SkillsName = (this.MoreSkills != "" && this.MoreSkills != undefined)?this.MoreSkills:[];
    form.value.Address = (form.value.Address == "" && form.value.Address != undefined)?this.UserData.Address:form.value.Address;
    form.value.BirthDate = moment(form.value.BirthDate).format('MM/DD/YYYY');
    form.value.ProfileImage = this.previewImage;
    form.value.ProfileImage = (this.UserData.ProfileImageURL !=null)?this.UserData.ProfileImageURL:this.previewImage;
    form.value.IsProfileImageUpdated = (this.UserData.ProfileImageURL != null)?false:true;
    this.showProfile = (form.value.ProfileImage == null)?true:false;
    form.value.UserType = this.myUserType;
    form.value.TimeZone = "";
    if (form.value.UserType == "Organizer") {  //As we are not showing skills and artist category if user is organizer so removed validators of skills and artist category.
      //  form.value.ArtistCategoryId = 0; 
      this.createProfileForm.controls['ArtistCategoryId'].clearValidators();
      this.createProfileForm.controls['ArtistCategoryId'].setErrors(null);
      this.createProfileForm.controls['ArtistSkills'].clearValidators();
      this.createProfileForm.controls['ArtistSkills'].setErrors(null);
      this.createProfileForm.controls['SkillsName'].clearValidators();
      this.createProfileForm.controls['SkillsName'].setErrors(null);
    }
   
    if (form.valid && this.showProfile != true && this.BirthDateError != true && this.duplicateErrorMsg !=true) {
      this.profileService.PostUserDetails(form.value).subscribe((data) => {
        this.ngxLoader.stop();
        if (data.IsUpdated == true) {
          const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 3000
          })
          Toast.fire({
            icon: 'success',
            title: 'Profile Saved Successfully'
          });
          this.router.navigate(['/view-profile']);
        }else {
          this.router.navigate(['/profile']);
        }
      }, error => {
        this.ngxLoader.stop();
        this.handleError(error);
      });
    }
    else {
      this.ngxLoader.stop();
    }
  }
  //end


    /**
   * @author : Snehal
   * @param fileInput
   * @function use : "generate base64 value of user profile image."
   * @date : 12-11-2019
   */
  async fileProgress(fileInput: any) {
    try {
      this.showProfile = false;
      this.UserData.ProfileImageURL = null;
      this.fileData = <File>fileInput.target.files[0];
      this.previewdata = await this.utilService.ImagePreview(this.fileData);
      let ArrayImg=this.previewdata.split(",");
      this.previewImage = ArrayImg[1];
    } catch (err) {
      console.log(err);
    }
  }
  //end



     /**
   * @author : Snehal
   * @param error
   * @function use : "Client and Server side Error handling "
   * @date : 13-11-2019
   */
  handleError(error) {
    if (error.error instanceof ErrorEvent) {
      // client-side error
      this.errorMessage = `Error: ${error.error.message}`;
    } else {
      this.isError = true;
      // server-side error     
      this.ShowErrorMsg = this.utilService.getErrorMessagebyStatus(error);
      this.errorMessage = this.ShowErrorMsg;// `${error.error.message}`;
      console.log(this.errorMessage)
      const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.errorMessage
      });
    }
  }
  //end
}

