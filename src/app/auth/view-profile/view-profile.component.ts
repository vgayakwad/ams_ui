import { Component, OnInit,Inject } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { ProfileService } from '../../services/profile.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import {UtilService} from '../../services/util.service';
import { Observable, throwError } from 'rxjs';
@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css'],
  providers: [ ProfileService]
})
export class ViewProfileComponent implements OnInit {


viewProfileImage:any;
UserData:any;
showArtist: Boolean=false;

localStorageLogin:any;
localStorageRegister:any;
errorMsg: String;
ShowErrorMsg: any;
profileId:any;
  constructor(private formBuilder: FormBuilder,private profileService: ProfileService,private route: ActivatedRoute,private router: Router,@Inject(LOCAL_STORAGE) private storage: StorageService,private ngxLoader: NgxUiLoaderService,
  private utilService:UtilService) { 

    this.profileId = atob(this.route.snapshot.paramMap.get('paramA'));
    console.log('profileId==',this.profileId);
  }

  ngOnInit() {
    this.ngxLoader.start();
    this.localStorageLogin = this.storage.get("login");
    this.localStorageRegister = this.storage.get("register");
    if(this.localStorageLogin != null){
      if(this.localStorageLogin.UserType == "Artist"){
        this.showArtist = true;
     }
    }
   if(this.localStorageRegister != null){
      if(this.localStorageRegister.UserType == "Artist"){
         this.showArtist = true;
      }
    }
    this.getUserDetails();
  }

    /**
   * @author : Snehal
   * @function use : "get artist or organizer profile details for populating that data on view screen "
   * @date : 21-10-2019
   */ 
  getUserDetails(){
    this.profileService.getUserDetails().subscribe((data)=>{
      this.ngxLoader.stop();
      console.log("ghghgh",data);
      this.UserData = data;
      this.viewProfileImage = this.UserData.ProfileImageURL;
      
     },error => {
      this.ngxLoader.stop();
      this.handleError(error);
      console.log("error",error);
     }); 
  }

    /**
   * @author : Snehal
   * @param error
   * @function use : "Client and Server side Error handling "
   * @date : 13-11-2019
   */ 
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
      console.log('client-side error', errorMessage)
    } else {
      // server-side error
      this.ShowErrorMsg= this.utilService.getErrorMessagebyStatus(error);
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      console.log('server-side error', errorMessage);
    const Toast = Swal.mixin({
      toast: true,
      showConfirmButton: false,
      timer: 3000
    })
    Toast.fire({
      icon: 'error',
      title: this.ShowErrorMsg
    });
    }

    // window.alert(errorMessage);
    return throwError(errorMessage);
  }
  //end

    /**
   * @author : Snehal
   * @function use : "on edit button click navigate to profile page. "
   * @date : 18-10-2019
   */ 
  editProfile(){
    //btoa(profileId)
    this.router.navigate(['/profile']);
  }
  //end

}
