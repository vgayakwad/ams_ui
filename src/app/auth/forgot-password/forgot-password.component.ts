import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators, FormControl } from '@angular/forms';
import {AuthServiceService} from '../../services/auth-service.service';
import {UtilService} from '../../services/util.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  ForgotPasswordForm:FormGroup;
  Email:FormControl;
  Password:FormControl;
  ShowErrorMsg: String;
  errorMsg: String;
  errorMessage :any;
  isError:boolean=false;
  isSame:boolean=false;
  PasswordErrorMsg:string;
  submitted:boolean = false;
  constructor(private router: Router,private formBuilder: FormBuilder,private authenticationService: AuthServiceService,private utilService:UtilService,
    private ngxLoader: NgxUiLoaderService) { }

  ngOnInit() {
    this.ForgotPasswordForm= this.formBuilder.group({
      Email:[null,[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      Password:[null,[Validators.required,Validators.minLength(6)]],
      Confirm_password:[null,[Validators.required,Validators.minLength(6)]]
    }, {
      validator: this.utilService.MustMatch('Password', 'Confirm_password')
  });
  }

  
  /**
   * @author : Snehal
   * @function use : "get form control name to apply validation in front end"
   * @date : 10-12-2019
   */
  get f() { return this.ForgotPasswordForm.controls; }
  //end


    /**
   * @author : Snehal
   * @param form
   * @function use : "Called forgot password api on submit. "
   * @date : 14-10-2019
   */
  onSubmit(form){
    this.ngxLoader.start();
    this.submitted =true;
    if(form.valid)
    {
      this.authenticationService.ResetPassword(form.value).subscribe(userRegisterdata=> {
      this.ngxLoader.stop();
      if(userRegisterdata.IsUpdated == true){
        const Toast = Swal.mixin({
          toast: true,
          showConfirmButton: false,
          timer: 3000
        })
        Toast.fire({
          icon: 'success',
          title: "Password Reset Successfully"
        });
        this.router.navigate(['/']);
      }
      else{
        //btoa('0')
        this.router.navigate(['/profile']);
      }
      this.router.navigate(['/Login']);
      this.ForgotPasswordForm.get('Email').setValue('');
      this.ForgotPasswordForm.get('Password').setValue('');
      this.ForgotPasswordForm.get('Confirm_password').setValue('');
       
    },error => {
      this.ngxLoader.stop();
      this.handleError(error);
  
    });
  }
  else{
    this.ngxLoader.stop();
    this.router.navigate(['/forgot-password']);
  }
  }
  //end

     /**
   * @author : Snehal
   * @param error
   * @function use : "Client and Server side Error handling "
   * @date : 13-11-2019
   */ 
  handleError(error) {
    if (error.error instanceof ErrorEvent) {
       // client-side error
      this.errorMessage = `Error: ${error.error.message}`;
     } else {
      this.isError=true;
       // server-side error
       this.ShowErrorMsg= this.utilService.getErrorMessagebyStatus(error);
        this.errorMessage = this.ShowErrorMsg;// `${error.error.message}`;
       const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        icon: 'error',
        title: this.errorMessage.Message
      });

     }
     //end
   }

     /**
   * @author : Snehal
   * @param event
   * @function use : "Restrick user to enter space as password. "
   * @date : 14-11-2019
   */
   _keyPress(event: any) {
    var k = event ? event.which : event.keyCode;
    if (k == 32) return false;
  }
  //end
}

